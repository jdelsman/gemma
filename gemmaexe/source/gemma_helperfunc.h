/****************************************************************
 *                                                              *
 *  GEMMA - End-member mixing in a GLUE framework               *
 *  Joost Delsman, Deltares / VU Amsterdam, 2012-2013           *
 *                                                              *
 *  Mersenne-Twister algorithm used in Monte-Carlo              *
 *  by Takuji Nishimura and Makoto Matsumoto. (see mtrnd_mt.h)  *
 *                                                              *
 ****************************************************************

Copyright (c) 2013, Deltares / VU Amsterdam
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef GEMMAHELPFUNC_H
#define GEMMAHELPFUNC_H

class rSettings;
class rEndmember;

#include "mtrnd_mt.h"
#include "mkl_lapack.h"

/* Auxiliary routines prototypes */
//void print_matrix( char* desc, MKL_INT m, MKL_INT n, double* a, MKL_INT lda );
//void print_vector_norm( char* desc, MKL_INT m, MKL_INT n, double* a, MKL_INT lda );
void print_settings(rSettings *, rEndmember **);
void print_secs(double diff, char *cDest);
void* memloc(const size_t size);
//int is_behavioural(double *b, int m, int n, double norm);
/*int min(int a, int b);
int max(int a, int b);
double min(double a, double b);
double max(double a, double b);*/
double gammavariate(MT::MersenneTwist *mtrnd, double alpha, double beta);
void get_fractions(int n, float *Fracs, MT::MersenneTwist *mtrnd);
int init_llsapprox(rSettings *pSet);

#endif // GEMMAREAD_H