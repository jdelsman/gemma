/****************************************************************
 *                                                              *
 *  GEMMA - End-member mixing in a GLUE framework               *
 *  Joost Delsman, Deltares / VU Amsterdam, 2012-2013           *
 *                                                              *
 *  Mersenne-Twister algorithm used in Monte-Carlo              *
 *  by Takuji Nishimura and Makoto Matsumoto. (see mtrnd_mt.h)  *
 *                                                              *
 ****************************************************************

Copyright (c) 2013, Deltares / VU Amsterdam
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#pragma warning(disable : 4996)

#include "gemma.h"
#include "gemma_helperfunc.h"
#include <stdio.h>
#include <math.h>

/* memory allocation, exits cleanly when error encountered */
void* memloc(const size_t size)
{
  void *pointer;

  pointer = malloc(size);
  if (pointer == NULL)
  {
    printf("GEMMA FATAL ERROR: Insufficient memory available\n");
    exit(1);
  }
  return pointer;
}

/* print settings to status screen */
void print_settings(rSettings *pSet, rEndmember **pEM)
{
   char fsize[12];

   printf("\nGEMMA parameters:\n");
   printf("- End-members (%d):    ", pSet->iNumEndmembers);
   for (int i=0;i<pSet->iNumEndmembers;i++)
   {
      printf("%s", pEM[i]->acName);
      if (i != pSet->iNumEndmembers-1) printf(", ");
   }
   if (pSet->bEndmembersRandom) printf(" (random, min %i)\n", pSet->iMinEndmembers);
   else printf("\n");
   printf("- Solutes (%d):        ", pSet->iNumSolutes);
   for (int i=0;i<pSet->iNumSolutes;i++)
   {
      printf("%s", pSet->acSoluteNames[i]);
      if (i != pSet->iNumSolutes-1) printf(", ");
   }
   if (pSet->bSolutesRandom) printf(" (random, min %i)\n", pSet->iMinSolutes);
   else printf("\n");
   printf("- Stream samples:     %d\n", pSet->iNumStreamSamples);
   printf("- Num iterations:     %d\n", pSet->lNumIter);
   if (pSet->bUseTempFile)
   {
      if (pSet->lNumIter < 10000000) sprintf(fsize, "%.1fMB", (pSet->blocksize*double(pSet->lNumIter))/1024./1024.);
      else sprintf(fsize, "%.1fGB",(pSet->blocksize*double(pSet->lNumIter))/1024./1024./1024.);
      
      if (pSet->bUseExistingTempFile) printf("- Use iter. tempfile: Yes (%s), existing\n", fsize);
      else if (pSet->bUseTempFile) printf("- Use iter. tempfile: Yes (%s), always new\n", fsize);
   }
   else printf("- Use iter. tempfile: No\n");
   if (pSet->bWithinTriangle) printf("- Within triangle:    Yes\n");
   else printf("- Within triangle: No\n");
   if (pSet->bUseLLSApprox) printf("- Use LLS approxim.:  Yes\n");
   else printf("- Use LLS approxim.:  No\n");
   if (pSet->bStopMaxBehave) printf("- Stop at max behav.: Yes\n");
   else printf("- Stop at max behav.: No\n");
   printf("- Max behavioural:    %d\n", pSet->lMaxBehavioural);
   printf("- Num threads:        %d\n", pSet->iNumThreads);
   if (pSet->bDiag) 
   {
      printf("- Diagnostics?:       Yes\n");
      printf("- Max diagnostics:    %d\n", pSet->lMaxDiag);
   }
   else printf("- Diagnostics?:       No\n", pSet->bDiag);
   
   printf("\n");
}   

/* Print no of seconds to string ?? days, ?? hrs, ?? mins, ?? sec (if applicable) */
void print_secs(double diff, char *cDest)
{
   int d,h,m,s,ms;
   
   ms = (int) (diff * 1000) % 1000;
   h = (int) diff % 86400;
   d = (int) diff / 86400;
   m = h % 3600;
   h = h / 3600;
   s = m % 60;
   m = m / 60;

   strcpy(cDest,"");
   if (d) sprintf(cDest, "%s%d days, ",cDest,d);
   if (h) sprintf(cDest, "%s%d hrs, ",cDest,h);
   if (m) sprintf(cDest, "%s%d mins, ",cDest,m);
   sprintf(cDest, "%s%d secs",cDest,s);
}

/* min / max functions * /
int min(int a, int b)
{
   if (a <= b) return a;
   else return b;
}

int max(int a, int b)
{
   if (a >= b) return a;
   else return b;
}

double min(double a, double b)
{
   if (a <= b) return a;
   else return b;
}

double max(double a, double b)
{
   if (a >= b) return a;
   else return b;
}
*/


double LOG4 = log(4.0);
double SG_MAGICCONST = 1.0 + log(4.5);
double gammavariate(MT::MersenneTwist *mtrnd, double alpha, double beta)
{
   /* From python random module:
      Uses R.C.H. Cheng, "The generation of Gamma
      variables with non-integral shape parameters",
      Applied Statistics, (1977), 26, No. 1, p71-74 */
   double ainv, bbb, ccc, v, x, z=1., r=1., u1, u2;
   
   ainv = sqrt(2.0 * alpha - 1.0);
   bbb = alpha - LOG4;
   ccc = alpha + ainv;

   do 
   {
      u1 = mtrnd->genrand_real2();
      if ((u1 < 1e-7) || u1 > .9999999) continue;
      u2 = 1.0 - mtrnd->genrand_real2();
      v = log(u1/(1.0-u1)) / ainv;
      x = alpha*exp(v);
      z = u1*u1*u2;
      r = bbb+ccc*v-x;
   }
   while ((r + SG_MAGICCONST - 4.5*z >= 0.0) || (r >= log(z)));
   return x * beta;
}

double expovariate(MT::MersenneTwist *mtrnd, double lambda)
{
   double u;

   u = mtrnd->genrand_real2();
   while (u < 1e-7)
   {
      u = mtrnd->genrand_real2();
   }
   return -log(u)/lambda;
}

/* Sample fractions from a symmetric Dirichlet distribution (with alpha=1),
   by sampling from gammavariate distributions and dividing by the sum of sampled values
   gammavariate distribution simplifies to expovariate distribution if alpha = 1 */
void get_fractions(int n, float *Fracs, MT::MersenneTwist *mtrnd)
{
   int i;
   double tot_frac = 0.0;

   for (i=0; i<n; i++)
   {
      Fracs[i] = (float) expovariate(mtrnd, 1.0 / double(n));
      tot_frac += Fracs[i];
   }
   for (i=0; i<n; i++) Fracs[i] *= (float) (1. / tot_frac);
}

/* initialize linear least squares matrix 
   find out size requirements for work array */
int init_llsapprox(rSettings *pSet)
{
   int i, m, n, nrhs, lda, ldb, lwork, info;
   double *a, *b, alpha, beta, wkopt;

   m = pSet->iNumSolutes + 1;
   n = pSet->iNumEndmembers;
   nrhs = 1;
   lda = m;
   ldb = m;
   a = (double *) memloc(lda*n * sizeof(double));
   b = (double *) memloc(ldb*nrhs * sizeof(double));
   for (i=0;i<lda*n;i++) a[i] = 0.0;
   for (i=0;i<ldb*nrhs;i++) b[i] = 0.0;
   alpha = 1.;
   beta = 0.;
   lwork = -1;
   dgels( "No transpose", &m, &n, &nrhs, a, &lda, b, &ldb, &wkopt, &lwork,
                   &info );
   free(a);
   free(b);
   return (int) wkopt;
}
