/****************************************************************
 *                                                              *
 *  GEMMA - End-member mixing in a GLUE framework               *
 *  Joost Delsman, Deltares / VU Amsterdam, 2012-2013           *
 *                                                              *
 *  Mersenne-Twister algorithm used in Monte-Carlo              *
 *  by Takuji Nishimura and Makoto Matsumoto. (see mtrnd_mt.h)  *
 *                                                              *
 ****************************************************************

Copyright (c) 2013, Deltares / VU Amsterdam
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#pragma warning(disable : 4996)
#include "gemma.h"
#include "gemma_read.h"
#include "gemma_helperfunc.h"
#include "mtrnd_mt.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <assert.h> 

void main (int argc, char* argv[])
{
   rSettings *Settings;
   rEndmember **EM;
   rStream **SW;
   int iS,iwork,t1,t2;
   long lChunk;
   double diff;
   char timestr[MAXLENGTH];
   FILE *fIter = NULL, *fOut = NULL;
   
   Settings = new rSettings;

   /* is an ini file given as argument? */
   if (argc > 1) strcpy(Settings->fnSettings, argv[1]);

   /* startup message */
   printf("**********************************************\n");
   printf("* GEMMA                                      *\n");
   printf("* End-member mixing in a GLUE Framework      *\n");
   printf("* (c) Joost Delsman, Deltares / VU Amsterdam *\n");
   printf("**********************************************\n\n");

   /* 1) read settings file */
   read_ini(Settings);

   /* 2) Read data on endmembers and stream data */
   EM = read_endmembers(Settings);
   SW = read_streamwater(Settings);
   fOut = open_output(Settings, EM); /* open random access output file and write header info*/

   /* print settings to screen */
   print_settings(Settings, EM);
	
   /* open tempfile */
   if (Settings->bUseTempFile) fIter = open_iter(Settings);

   /* If LLS is used, init LLS approximation */
   if (Settings->bUseLLSApprox) iwork = init_llsapprox(Settings);

   /* max number of jobs to save */
   Settings->lMaxSave = Settings->bDiag ? Settings->lMaxDiag : Settings->lMaxBehavioural;

   /* overall calculation time */
   t1 = clock();

   /* 3) Set up Monte Carlo end-member mixing for each stream water sample */
   /*    Fork a team of threads with each thread having a private tid variable, handle one sample per thread */
   /*    Loop over all stream water samples */
   #pragma omp parallel
   {
      /* declare thread private variables */
	  MT::MersenneTwist mtrnd;
	  rJob *Job, **SavedJobs, **IterJobs;
	  bool done;
      int i,j,n,tid,t3,t4, *ChunkInt;
      long lJobID,nbehave=0,ldoneat,*ChunkLong;
	  float sumLH=0, *ChunkFloat;
      double diff=0, *work, *a, *b, *c;
	  char timestr[MAXLENGTH];

	  /* Obtain thread id and use to seed MersenneTwister */
      tid = omp_get_thread_num();

      /* seed MersenneTwister */
      mtrnd.init_genrand(tid);

      /* Allocate and matrix calculation workspace */
      if (Settings->bUseLLSApprox) work = (double*) memloc( iwork*sizeof(double) );
      a = (double *) memloc((Settings->iNumSolutes + 1)*Settings->iNumEndmembers * sizeof(double));
      b = (double *) memloc((Settings->iNumEndmembers) * sizeof(double));
      c = (double *) memloc((Settings->iNumSolutes + 1) * sizeof(double));
	  //c = (double *) memloc((Settings->iNumSolutes) * sizeof(double));
	  //a = new double[(Settings->iNumSolutes + 1)*Settings->iNumEndmembers];
	  //b = new double[Settings->iNumEndmembers];
	  //c = new double[Settings->iNumSolutes + 1];

      /* Allocate iter jobs array */
      IterJobs = new rJob *[CHUNKSIZE];

      /* IF USING TEMPORARY ITERATIONS FILE: */
	  if (Settings->bUseTempFile)
      {
         /* FIRST: CREATE R-A FILE OF ITERATIONS, OR SKIP IF READ FROM EXISTING */
         /* Every thread creates CHUNKSIZE of Jobs, solves stream concentrations and saves to file */

         /* allocate chunk arrays */
         ChunkLong  = (long*)  memloc(CHUNKSIZE*sizeof(long));
         ChunkInt   = (int*)   memloc(CHUNKSIZE*2*sizeof(int));
         ChunkFloat = (float*) memloc(CHUNKSIZE*((Settings->iNumEndmembers+1)*Settings->iNumSolutes+Settings->iNumEndmembers)*sizeof(float));
      
         if (! Settings->bUseExistingTempFile)
         {
            # pragma omp single
            printf("Building temporary iteration database");

            #pragma omp for schedule(dynamic)
            for (lChunk=0;lChunk<(Settings->lNumIter / CHUNKSIZE);lChunk++)
            {
               /* print time indicator */
               if ((Settings->lNumIter / CHUNKSIZE) > 20)
                  if ((lChunk+1) % (long)((Settings->lNumIter / CHUNKSIZE) / 20.0) == 0) printf(".");

               for (i=0;i<CHUNKSIZE;i++)
               {
                  /* create job for new iteration: draw endmember fractions and concentrations and calculate stream conc */
                  IterJobs[i] = new rJob(true, long(lChunk*CHUNKSIZE+i), Settings, EM, &mtrnd);

                  /* calculate stream concentrations */
                  IterJobs[i]->calc_stream(a, b, c);
               }

               /* fill chunk to long*1, int*2, float*(Settings->iNumEndmembers+1)*Settings->iNumSolutes+Settings->iNumEndmembers arrays */
               for (i=0;i<CHUNKSIZE;i++)
               {
                  IterJobs[i]->fill_chunk(i,&ChunkLong,&ChunkInt,&ChunkFloat);
               }

               /* write chunk to file, one thread at a time */
               #pragma omp critical(ChunkIO)
               {
                  n = ra_write(fIter, ChunkLong, CHUNKSIZE, sizeof(long));
                  n = ra_write(fIter, ChunkInt, 2*CHUNKSIZE, sizeof(int));
                  n = ra_write(fIter, ChunkFloat,((Settings->iNumEndmembers+1)*Settings->iNumSolutes+Settings->iNumEndmembers)*CHUNKSIZE, sizeof(float));
                  fflush(fIter);
               }

               /* free mem */
               for (i=0;i<CHUNKSIZE;i++) delete IterJobs[i];
            }   
            #pragma omp single
            printf("done.\n\n");
         }
         else
         {
            #pragma omp single
            printf("Using temporary iteration database %s.\n\n",Settings->fnIter);
         }
      } /* END TEMPORARY ITERATIONS FILE */

      /* THEN: FOR EVERY SAMPLE:
         READ CHUNKS OF R-A FILE IN MEM, EVALUATE CALC STREAM CONC TO SAMPLE
         IF BEHAVIOURAL: KEEP, ELSE DISCARD */
      #pragma omp single
      printf("Start calculation for %i samples...\n",Settings->iNumStreamSamples);
      
      #pragma omp for schedule(dynamic)
      for (iS=0;iS<Settings->iNumStreamSamples;iS++)  /* each thread handles a stream water sample */
      {
         /* reset nbehave and lJobID */
         nbehave = 0;
         lJobID = 0;
         done = false;
         sumLH = 0.;
         t3 = clock();

         //printf("iS: %i, thread: %i\n",iS,tid);

         /* Allocate saved jobs array */
         SavedJobs = new rJob *[Settings->lMaxSave];

         /* Monte Carlo iteration loop: */
         for (lChunk=0;lChunk<(Settings->lNumIter / CHUNKSIZE) && !done;lChunk++)
         {
            /* Init chunk of Jobs, either from tempfile or scratch */
            if (Settings->bUseTempFile)
            {
               /* read chunk from tempfile, only one thread at a time */
               #pragma omp critical(ChunkIO)
               {
                  /* set file pointer and read 3 chunks */
                  _fseeki64(fIter, (__int64) lChunk * CHUNKSIZE * Settings->blocksize, SEEK_SET);
                  n = ra_read(fIter, ChunkLong, CHUNKSIZE, sizeof(long));
                  n = ra_read(fIter, ChunkInt, 2*CHUNKSIZE, sizeof(int));
                  n = ra_read(fIter, ChunkFloat,((Settings->iNumEndmembers+1)*Settings->iNumSolutes+Settings->iNumEndmembers)*CHUNKSIZE, sizeof(float));
               }

               /* init Jobs from chunk */
               for (i=0;i<CHUNKSIZE;i++)
               {
                  IterJobs[i] = new rJob(false, NULL, Settings, NULL, NULL,i,ChunkLong,ChunkInt,ChunkFloat);
               }
            }
            else
            {
               /* init Jobs from scratch */
               for (i=0;i<CHUNKSIZE;i++)
               {
                  /* create job for new iteration: draw endmember fractions and concentrations and calculate stream conc */
                  IterJobs[i] = new rJob(true, long(lChunk*CHUNKSIZE+i), Settings, EM, &mtrnd);//, NULL, NULL, NULL, NULL);
                  
                  /* calculate stream concentrations */
                  IterJobs[i]->calc_stream(a, b, c);
               }
            }

            /* IF bUseLLS: approximate fractions and recalculate stream concentrations */
            if (Settings->bUseLLSApprox)
            {
               for (i=0;i<CHUNKSIZE;i++)
               {
                  IterJobs[i]->llsapprox_fractions(iwork, work, a, b, c, SW[iS]);
               }
            }

            /* calculate likelihood for each Job in chunk */
            for (i=0;i<CHUNKSIZE;i++)
            {            
               Job = IterJobs[i];

               /* calc residuals and likelihood */
               Job->calc_residuals(SW[iS]);
               sumLH += Job->calc_likelihood(SW[iS], tid);
               nbehave += long(Job->bBehave);

               /* keep if behavioural or diagnostics, else delete. */
               if ((Job->bBehave || Settings->bDiag) && (lJobID < Settings->lMaxSave))
               {
                  assert(lJobID < Settings->lMaxSave);
                  SavedJobs[lJobID] = Job;
                  lJobID++;
                  
                  /* if bStopMaxBehave: raise stop flag if enough behaviourals are found */
                  if (Settings->bStopMaxBehave && lJobID >= Settings->lMaxSave) 
                  {
                     done = true;
                     ldoneat = lChunk*CHUNKSIZE+i;

                     /* free mem */
                     for (int j=i;j<CHUNKSIZE;j++) delete IterJobs[j];
                     break;
                  }
               }
               else delete Job;  /////// HIER CRASHT IE!!!
            } /* End chunk loop */
         } /* End Monte Carlo Iteration loop */
         
         /* time sample */
         t4 = clock();
         diff = (double) (t4 - t3) / CLOCKS_PER_SEC;
         print_secs(diff, timestr);
         /* one thread at a time reports prints end message and writes sample header --> samples no longer saved in order!! */
         #pragma omp critical(WriteSample)
         {
            if (done)
            {
               /* predict nbehave if stopped beforehand */
               nbehave = (long) (nbehave * (Settings->lNumIter / double(ldoneat)));
               /* done iterating... */
               printf("Sample %s done (~%d behavioural, %s)\n", SW[iS]->acName, nbehave, timestr);
            }
            else
            {
               /* done iterating... */
               printf("Sample %s done (%d behavioural, %s)\n", SW[iS]->acName, nbehave, timestr);
            }

            /* write behavioural records (if diagnostics: all) to binary output file */
            /* first record: int strlen name, strlen datetime, int n_behaviourals int ncols */
            ra_writestring(fOut, SW[iS]->acName);
            ra_writestring(fOut, SW[iS]->acDatetime);
            ra_write(fOut, &nbehave, 1, sizeof(long));
            ra_write(fOut, &lJobID);
         
            /* write data */
            i = 0;
            for (i=0;i<lJobID;i++)
            {
               SavedJobs[i]->write(fOut);
            }
         }

         /* free mem */
         for (i=0;i<lJobID;i++) delete SavedJobs[i];
         delete[] SavedJobs;
      } /* End sample loop */

      /* free thread-private mem */
	  if (Settings->bUseLLSApprox) free(work);
	  //delete[] a;
	  //delete[] b;
	  //delete[] c;
      free(a);
      free(b);
      free(c);
      if (Settings->bUseTempFile)
      {
         free(ChunkLong);
         free(ChunkInt);
         free(ChunkFloat);
      }
      delete[](IterJobs);
   }  /* All threads join master thread and terminate */

   /* free arrays and classes */
   if (Settings->bUseTempFile) fclose(fIter);
   fclose(fOut);
   for (int i=0;i<Settings->iNumEndmembers;i++) delete EM[i];
   delete[] EM;
   for (int i=0;i<Settings->iNumStreamSamples;i++) delete SW[i];
   delete[] SW;
   delete Settings;

   t2 = clock(); /* time overall */
   diff = (double) (t2 - t1) / CLOCKS_PER_SEC;
   print_secs(diff, timestr);
   printf("\nTotal run time: %s.\n",timestr);
}