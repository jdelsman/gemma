/****************************************************************
 *                                                              *
 *  GEMMA - End-member mixing in a GLUE framework               *
 *  Joost Delsman, Deltares / VU Amsterdam, 2012-2013           *
 *                                                              *
 *  Mersenne-Twister algorithm used in Monte-Carlo              *
 *  by Takuji Nishimura and Makoto Matsumoto. (see mtrnd_mt.h)  *
 *                                                              *
 ****************************************************************

Copyright (c) 2013, Deltares / VU Amsterdam
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#pragma warning(disable : 4996)
#include "gemma.h"
#include "gemma_read.h"
#include "gemma_helperfunc.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include <omp.h>

#define NCOMP 5

void strtoupper(char *sPtr)
{
   /* Function to convert string to uppercase */
   while ( *sPtr != '\0' ) 
   {
      *sPtr = toupper ( ( unsigned char ) *sPtr );
      ++sPtr;
   }
}

short split(char ***acSubStrs, char *acLine, char cSplit)
{
   /* Function to split string in substrings by split chars 
      caller responsible for freeing memory! */
   short i=0;
   short nValues = 1;
   char **acLocal;
   char *p, *pold=NULL;

   /* Count number of splits in line */ 
   for (i=0; i < (short)strlen(acLine); i++)
   {
      if (acLine[i] == cSplit) nValues++;
   }
   
   /* Reserve memory 1 */
   acLocal = (char **) memloc(nValues * sizeof(char *));
   
   /* Split line in char arrays */
   p = acLine;
   pold = p;
   i = 0;
   do
   {
      ++p;
      if (p[0] == cSplit || p[0] == '\n')
      {
         acLocal[i] = (char *) memloc((p-pold+1)*sizeof(char));
         strncpy(acLocal[i], pold, p-pold);
         acLocal[i][p-pold] = '\0';
         i++;
         pold = p+1;
      }
   } while (p[0]!='\n');
   *acSubStrs = acLocal;
   return i;
}

void freesub(char **p, int n)
{
   for (int i=0;i<n;i++) free(p[i]);
   free(p);
}

extern void read_ini(rSettings *pSet)
{
   /* Function to init settings to default, and read settings file */
   /* Joost, 22/3 TODO: crashes if last item does not end with /n */
   FILE *fIni;
   int n;
   char acLine[MAXLENGTH], acComp[MAXLENGTH], sep='\t';
   char **acSubStrs;                              /* splitted strings */
   float dVal;

   /* Read settings file */
   if (fIni = fopen(pSet->fnSettings,"r"))
   {
      printf("Reading settings file %s\n",pSet->fnSettings);
      while (!feof(fIni))
      {
         /* read line, exclude comments and empty lines */
         if (fgets(acLine, MAXLENGTH, fIni) != NULL && strlen(acLine) > NCOMP && acLine[0] != '!')
         {
            /* compare first NCOMP chars, in uppercase */
            n = split(&acSubStrs, acLine, sep);

            if (n > 1)
            {
               strcpy(acComp, acSubStrs[0]);
               strtoupper(acComp);
               
               /* filename endmembers */
               if (strncmp(acComp, "FNEND", 5) == 0)
               {
                  strcpy(pSet->fnEndmembers, acSubStrs[1]);
               }
               /* filename stream */
               else if (strncmp(acComp, "FNSTR", 5) == 0)
               {
                  strcpy(pSet->fnStream, acSubStrs[1]);
               }
               /* filename output */
               else if (strncmp(acComp, "FNOUT" ,5) == 0)
               {
                  strcpy(pSet->fnOutput, acSubStrs[1]);
               }
               /* filename output */
               else if (strncmp(acComp, "FNITER" ,6) == 0)
               {
                  strcpy(pSet->fnIter, acSubStrs[1]);
               }
               /* num iterations */
               else if (strncmp(acComp, "NITER", 5) == 0)
               {
                  sscanf(acSubStrs[1], "%g", &dVal);
                  pSet->lNumIter = (long) dVal;
               }
               /* num threads */
               else if (strncmp(acComp, "NTHRE", 5) == 0)
               {
                  pSet->iNumThreads = atoi(acSubStrs[1]);
               }
               /* max behavioural */
               else if (strncmp(acComp, "MAXBE", 5) == 0)
               {
                  pSet->lMaxBehavioural = atol(acSubStrs[1]);
               }
               /* num endmembers */
               else if (strncmp(acComp, "NENDM", 5) == 0)
               {
                  pSet->iNumEndmembers = atoi(acSubStrs[1]);
               }
               /* minimum n endmembers */
               else if (strncmp(acComp, "MINEN", 5) == 0)
               {
                  pSet->iMinEndmembers = atoi(acSubStrs[1]);
               }
               /* num solutes */
               else if (strncmp(acComp, "NSOLU", 5) == 0)
               {
                  pSet->iNumSolutes = atoi(acSubStrs[1]);
               }
               /* minimum n solutes */
               else if (strncmp(acComp, "MINSO", 5) == 0)
               {
                  pSet->iMinSolutes = atoi(acSubStrs[1]);
               }
               /* RSS norm */
               else if (strncmp(acComp, "RSSNO", 5) == 0)
               {
                  pSet->dNormRSS = atof(acSubStrs[1]);
               }
               /* norm factor 1 */
               else if (strncmp(acComp, "NORMFACT1", 9) == 0)
               {
                  pSet->dNormFact1 = atof(acSubStrs[1]);
               }
               /* norm factor 2 */
               else if (strncmp(acComp, "NORMFACT2", 9) == 0)
               {
                  pSet->dNormFact2 = atof(acSubStrs[1]);
               }
               /* wbal norm */
               else if (strncmp(acComp, "WBALN", 5) == 0)
               {
                  pSet->dNormWbal = atof(acSubStrs[1]);
               }
               /* wbal factor */
               else if (strncmp(acComp, "WBALF", 5) == 0)
               {
                  pSet->dWbalFact = atof(acSubStrs[1]);
               }
               /* solute names */
               else if (strncmp(acComp, "SOLUT", 5) == 0)
               {
                  if (n == pSet->iNumSolutes+1)
                  {
                     pSet->acSoluteNames = (char **) memloc(pSet->iNumSolutes * sizeof(char *));
                     for (int i=1;i<n;i++)
                     {
                        pSet->acSoluteNames[i-1] = (char *) memloc((strlen(acSubStrs[i]) + 1) * sizeof(char));
                        strcpy(pSet->acSoluteNames[i-1],acSubStrs[i]);
                     }
                  }
                  else
                  {
                     printf("Error: number of solutes (%d) does not match nr of solute names (%d)\n",pSet->iNumSolutes,n-1);
                     exit(1);
                  }
               }
               /* diagnostics */
               else if (strncmp(acComp, "DIAGN", 5) == 0)
               {
                  strtoupper(acSubStrs[1]);
                  if (strncmp(acSubStrs[1], "T", 1) == 0) pSet->bDiag = true;
               }
               /* diagnostics */
               else if (strncmp(acComp, "MAXDI", 5) == 0)
               {
                  pSet->lMaxDiag = atol(acSubStrs[1]);
               }
               /* random end-members */
               else if (strncmp(acComp, "RANDOME", 7) == 0)
               {
                  strtoupper(acSubStrs[1]);
                  if (strncmp(acSubStrs[1], "T", 1) == 0) pSet->bEndmembersRandom = true;
               }
               /* random solutes */
               else if (strncmp(acComp, "RANDOMS", 7) == 0)
               {
                  strtoupper(acSubStrs[1]);
                  if (strncmp(acSubStrs[1], "T", 1) == 0) pSet->bSolutesRandom = true;
               }
               /* use least squares approximation for end member fractions */
               else if (strncmp(acComp, "USELLS", 6) == 0)
               {
                  strtoupper(acSubStrs[1]);
                  if (strncmp(acSubStrs[1], "T", 1) == 0) pSet->bUseLLSApprox = true;
               }
               else if (strncmp(acComp, "WITHI", 5) == 0)
               {
                  strtoupper(acSubStrs[1]);
                  if (strncmp(acSubStrs[1], "T", 1) == 0) pSet->bWithinTriangle = true;
               }
               else if (strncmp(acComp, "STOP", 4) == 0)
               {
                  strtoupper(acSubStrs[1]);
                  if (strncmp(acSubStrs[1], "T", 1) == 0) pSet->bStopMaxBehave = true;
               }
               else if (strncmp(acComp, "USETEMP", 7) == 0)
               {
                  strtoupper(acSubStrs[1]);
                  if (strncmp(acSubStrs[1], "F", 1) == 0) pSet->bUseTempFile = false;
               }
               else if (strncmp(acComp, "USEEXIS", 7) == 0)
               {
                  strtoupper(acSubStrs[1]);
                  if (strncmp(acSubStrs[1], "T", 1) == 0) pSet->bUseExistingTempFile = true;
               }
            }
            freesub(acSubStrs,n);
         }
      }
      fclose(fIni);

      /* set settings */
      if (pSet->lMaxBehavioural == -1) pSet->lMaxBehavioural = pSet->lNumIter;
      if (pSet->lMaxDiag == -1) pSet->lMaxDiag = pSet->lNumIter;
      if (pSet->dNormWbal == 0) pSet->dNormWbal = pSet->dNormRSS;
	  /* enforce minimum nr of endmembers / solutes to be less/equal than nr of endm/solutes */
	  pSet->iMinEndmembers = std::min(pSet->iMinEndmembers,pSet->iNumEndmembers);
	  pSet->iMinSolutes = std::min(pSet->iMinSolutes,pSet->iNumSolutes);
	  /* enforce numthreads to be less or equal than max nr of threads */
	  pSet->iNumThreads = std::min(pSet->iNumThreads,omp_get_max_threads()); 
      omp_set_num_threads(pSet->iNumThreads);
      /* enforce num iterations to be minimum and multiple of CHUNK */
      pSet->lNumIter = int((pSet->lNumIter + CHUNKSIZE-1) / CHUNKSIZE) * CHUNKSIZE;
      /* size of datablock in iteration file */
      if (pSet->bUseTempFile)
      {
         pSet->blocksize = (size_t) sizeof(long) + 2 * sizeof(int);
         pSet->blocksize += (size_t) ((pSet->iNumEndmembers+1)*pSet->iNumSolutes+pSet->iNumEndmembers) * sizeof(float);
      }
   }
   else
   {
      printf("Unable to open settings file %s, using defaults...\n",pSet->fnSettings);
   }
}

extern rEndmember **read_endmembers(rSettings *pSet)
{
   /* Function to read endmembers file */
   FILE *fEM;
   rEndmember **EM;
   int i, iEM=0, n, iSol=0;
   char acLine[MAXLENGTH], *acName, *token, *next_token;
   char **acSubStrs = NULL;                              /* splitted strings */ 
   //std::tr1::mt19937 MT;

   
   EM = new rEndmember *[pSet->iNumEndmembers];

   /* Read endmembers file */
   if (fEM = fopen(pSet->fnEndmembers, "r"))
   {
      printf("Reading endmembers file %s\n",pSet->fnEndmembers);
      while (!feof(fEM) && iEM<pSet->iNumEndmembers)
      {
         if (fgets(acLine, MAXLENGTH, fEM) != NULL)
         {
            /* read until '[' is encountered */
            if (acLine[0] == '[')
            {
               /* endmember encountered */
               token = strtok_s(acLine, "[]", &next_token);
               acName = (char *) memloc((strlen(token)+1)*sizeof(char));
               strcpy(acName, token);
               EM[iEM] = new rEndmember(acName, pSet);
               free(acName);
               
               /* loop over (tab-separated) table with solutes and uncertainty
                  format: Solute Distribution(U/G)	Min	Max	Mean	Std*/
               i = 0;
               while (i < EM[iEM]->nSolutes && !feof(fEM))
               {
                  if (fgets(acLine, MAXLENGTH, fEM) != NULL  && acLine[0] != '!')
                  {
                     if (acLine[0] == '[')
                     {
                        printf("End-member data for %i solutes missing, exiting.\n", EM[iEM]->nSolutes-i);
                        fclose(fEM);
                        exit(1);
                     }
                     
                     n = split(&acSubStrs, acLine, '\t');
                     iSol = EM[iEM]->get_index(acSubStrs[0]);
                     if (iSol > -1)
                     {
                        strcpy(EM[iEM]->acSoluteNames[iSol], acSubStrs[0]);
                        strcpy(EM[iEM]->aSolutes[iSol].acSolute, acSubStrs[0]);
                        EM[iEM]->aSolutes[iSol].cDist = acSubStrs[1][0];
                        EM[iEM]->aSolutes[iSol].dMin = atof(acSubStrs[2]);
                        EM[iEM]->aSolutes[iSol].dMax = atof(acSubStrs[3]);
                        EM[iEM]->aSolutes[iSol].dMean = atof(acSubStrs[4]);
                        EM[iEM]->aSolutes[iSol].dStd = atof(acSubStrs[5]);
                        i++;
                     }
                     freesub(acSubStrs,n);
                  }
               }
               //EM[iEM] = new rEndmember(acName, 3, acSolutes)
               iEM++;
            }
         }
      }
      fclose(fEM);
   }
   else
   {
      printf("Unable to open endmembers file %s, exiting.\n",pSet->fnEndmembers);
      exit(1);
   }
   return EM;
}

extern rStream **read_streamwater(rSettings *pSet)
{
   /* Function to read streamwater file */
   FILE *fSW;
   rStream **SW;
   int nSamples=0, nItems=0, iSW=0, iSol=0;
   char acLine[MAXLENGTH];
   char **acHeader=NULL, **acSubStrs = NULL;                              /* splitted strings */ 
   
   /* Read streamwater file */
   if (fSW = fopen(pSet->fnStream, "r"))
   {
      printf("Reading streamwater file %s\n",pSet->fnStream);
      
      /* first two lines: 1) n samples, 2) header */
      while (fgets(acLine, MAXLENGTH, fSW) == NULL || acLine[0] == '!');  /* skip empty lines and comment lines (start with !) */
      pSet->iNumStreamSamples = atoi(acLine);

      /* allocate array */
      SW = new rStream *[pSet->iNumStreamSamples];
   
      while (fgets(acLine, MAXLENGTH, fSW) == NULL || acLine[0] == '!'); /* skip empty lines and comment lines (start with !) */

      /* header should be: sample code \t datetime \t solute 1 \t std solute 1 \t ... solute n \t std solute n */
      nItems = split(&acHeader, acLine, '\t');

      /* read rest of streamwater file */
      iSW=0;
      while (!feof(fSW) && iSW < pSet->iNumStreamSamples)
      {
         if (fgets(acLine, MAXLENGTH, fSW) != NULL && acLine[0] != '!')
         {
            /* split line, equal colums as header? */
            if (split(&acSubStrs, acLine, '\t') == nItems)
            {
               /* new streamwater sample */
               SW[iSW] = new rStream(acSubStrs[0], acSubStrs[1], pSet);
               
               /* loop over columns: is solute part of emma? define solute in sample */
               for (int i=2;i<nItems;i=i+2)
               {
                  iSol = SW[iSW]->get_index(acHeader[i]);
                  if (iSol > -1)
                  {
                     SW[iSW]->aSolutes[iSol].cDist = 'G'; // stream water always gaussian
                     SW[iSW]->aSolutes[iSol].dMean = atof(acSubStrs[i]);
                     SW[iSW]->aSolutes[iSol].dStd = atof(acSubStrs[i+1]);
                  }
               }
               iSW++;
            }
         }
      }
      fclose(fSW);
      freesub(acHeader, nItems);
   }
   else
   {
      printf("Unable to open stream data file %s, exiting.\n",pSet->fnStream);
      exit(1);
   }
   return SW;
}

extern int ra_write(FILE *f, void *p, int n, size_t size)
{
   return (int) fwrite((char *) p, size, n, f);
}

extern int ra_read(FILE *f, void *p, int n, size_t size)
{
   return (int) fread((void *) p, size, n, f);
}

/* write a string to random access. Write length int first, then the string */
extern void ra_writestring(FILE *f, const char *s)
{
   int l;
   l = (int) strlen(s);
   ra_write(f, &l);
   ra_write(f, (void *) s, l, sizeof(char));
}

extern FILE *open_output(rSettings *pSet, rEndmember **pEM)
{
   FILE *fOut;
   int i;
   bool x64=false, writesh=false;
#ifdef _M_X64 
   x64 = true;
#endif

   /* open random access output file and write header info*/
   if (fOut = fopen(pSet->fnOutput,"wb"))
   {
      printf("Writing output to %s\n", pSet->fnOutput);
      ra_write(fOut, &x64, 1, sizeof(bool)); /* store if 64bit, to handle different size of long */
      ra_write(fOut, &pSet->iNumStreamSamples);
      ra_write(fOut, &pSet->lNumIter, 1, sizeof(long));
      ra_write(fOut, &pSet->iNumEndmembers);
      for (i=0;i<pSet->iNumEndmembers;i++) ra_writestring(fOut, pEM[i]->acName);
      ra_write(fOut, &pSet->iNumSolutes);
      for (i=0;i<pSet->iNumSolutes;i++) ra_writestring(fOut, pSet->acSoluteNames[i]);
      ra_write(fOut, &pSet->bDiag, 1, sizeof(bool));
      ra_write(fOut, &pSet->bUseLLSApprox, 1, sizeof(bool));
      ra_write(fOut, &pSet->bWithinTriangle, 1, sizeof(bool));
      ra_write(fOut, &pSet->bStopMaxBehave, 1, sizeof(bool));
      ra_write(fOut, &writesh, 1, sizeof(bool));
      return fOut;
   }
   else
   {
      printf("Unable to open output file %s, exiting.\n", pSet->fnOutput);
      exit(1);
      return NULL;
   }
}

extern FILE *open_iter(rSettings *pSet)
{
   FILE *fIter;
   __int64 sz;
   /* open random access iterations file with correct attributes */
   /* first: if use existing, try to open with rb. Fails: set use existing to false and try again */
   if (pSet->bUseExistingTempFile)
   {
      if (!(fIter = fopen(pSet->fnIter,"rb"))) 
      {
         printf("Unable to open iterations file %s, building new\n", pSet->fnIter);
         pSet->bUseExistingTempFile = false;
      }
      else
      {
         /* check if filesize is correct */
         _fseeki64(fIter, 0L, SEEK_END);
         sz = _ftelli64(fIter);
         _fseeki64(fIter, 0L, SEEK_SET);
         if (sz != pSet->blocksize*pSet->lNumIter)
         {
            printf("Incorrect filesize iterations file %s, building new\n", pSet->fnIter);
            pSet->bUseExistingTempFile = false;
            fclose(fIter);
         }
      }
   }

   /* if not use existing, open with w+b */
   if (! pSet->bUseExistingTempFile)
   {
      if (! (fIter = fopen(pSet->fnIter,"w+b")))
      {
         printf("Unable to open iterations file %s, exiting.\n", pSet->fnIter);
         exit(1);
         return NULL;
      }
   }
   return fIter;
}