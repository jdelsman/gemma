/****************************************************************
 *                                                              *
 *  GEMMA - End-member mixing in a GLUE framework               *
 *  Joost Delsman, Deltares / VU Amsterdam, 2012-2013           *
 *                                                              *
 *  Mersenne-Twister algorithm used in Monte-Carlo              *
 *  by Takuji Nishimura and Makoto Matsumoto. (see mtrnd_mt.h)  *
 *                                                              *
 ****************************************************************

Copyright (c) 2013, Deltares / VU Amsterdam
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#pragma warning(disable : 4996)
#include "gemma.h"
#include "gemma_helperfunc.h"
#include "gemma_read.h"
#include "mtrnd_mt.h"
#include "mkl_lapack.h"
#include "mkl_blas.h"
#include <math.h>
#include <malloc.h>

#define MAXLENGTH 512

/* class functions too big to define inline */

/* Functions for class rSolute */
double rSolute::get_value(MT::MersenneTwist *mtrnd)
{
   double  u1,u2,z,zz;
   
   if (cDist == 'U')
   {
      return dMin + mtrnd->genrand_real1() * (dMax - dMin);
   }
   else if (cDist == 'G')
   {
      /* Code from python random module:
      # Uses Kinderman and Monahan method. Reference: Kinderman, A.J. and Monahan, J.F., "Computer generation of random
      # variables using the ratio of uniform deviates", ACM Trans Math Software, 3, (1977), pp257-260.
      */
      while (true)
      {
          u1 = mtrnd->genrand_real2();
          u2 = 1.0 - mtrnd->genrand_real2();
          z = nvmc * (u1 - 0.5) / u2;
          zz = z * z / 4.0;
          if (zz <= -log(u2))
              break;
      }
      return dMean + z*dStd;
   }
   return -1.;
}

/* Functions for class rSample */
int rSample::get_index(const char *Solute) const
{
   for (int i=0;i<nSolutes;i++)
   {
      if (strcmp(Solute, acSoluteNames[i]) == 0) return i;
   }
   return -1;
}

/* Functions for class rJob */
rJob::rJob(bool bI, long lID, rSettings *pS, rEndmember **EM, MT::MersenneTwist *mtrnd, int i,long *ChunkLong,int *ChunkInt,float *ChunkFloat)
{
   pSet = pS;
   bInit = bI;
   if (bInit) InitIter(lID, EM, mtrnd);
   else ReadIter(i,ChunkLong,ChunkInt,ChunkFloat);

   /* reset vals */
   LH = 0;
   fWbalCalc = (float) pSet->dWbalFact;
   bBehave = false;
   e=0;
   s=0;
   iEM=0;
   iSol=0;
}

void rJob::InitIter(long lID, rEndmember **EM, MT::MersenneTwist *mtrnd)
{
   /* Init rJob for iteration array:
      - determine nEndm and nSol
      - determine which endmembers and solutes
      - allocate arrays
      - draw values
      */
   float     *Fracs;     /* calculated fractions of endmembers in job (size: nEndm) */

   lJobID = lID;

   /* allocate bool end-member / solute arrays */
   bEM = (bool *) memloc(pSet->iNumEndmembers * sizeof(bool));
   for (e=0;e<pSet->iNumEndmembers;e++) bEM[e] = true;
   bSol = (bool *) memloc(pSet->iNumSolutes * sizeof(bool));
   for (s=0;s<pSet->iNumSolutes;s++) bSol[s] = true;

   /* First: how many and which endmembers */
   if (pSet->bEndmembersRandom)
   {
      nEndmembers = int(0.5 + pSet->iMinEndmembers + mtrnd->genrand_real1() * (pSet->iNumEndmembers - pSet->iMinEndmembers));
      for (e=0; e<(pSet->iNumEndmembers - nEndmembers); e++)
      {
         iEM = int(0.5 + mtrnd->genrand_real2() * (double) pSet->iNumEndmembers);
         if (iEM == pSet->iNumEndmembers) iEM--; /* make sure iEM after rounding not outside array */
         while (bEM[iEM] == false)
         {
            /* shift one to the right, if at end, cycle */
            iEM++;
            if (iEM == pSet->iNumEndmembers) iEM = 0;
         }
         bEM[iEM] = false;
      }
   }
   else nEndmembers = pSet->iNumEndmembers;

   /* Second: how many and which solutes. Minimum solutes = max( minimum solutes in settings, number of endmembers) */
   if (pSet->bSolutesRandom)
   {
      nSolutes = int( 0.5 + std::max(pSet->iMinSolutes, nEndmembers) + mtrnd->genrand_real1() * (pSet->iNumSolutes - std::max(pSet->iMinSolutes, nEndmembers)));
      for (s=0; s<(pSet->iNumSolutes - nSolutes); s++)
      {
         //iSol = int(0.5 + mtrnd->genrand_real2() * (double) pSet->iNumSolutes);  /* which solute is excluded? */
		 iSol = int(mtrnd->genrand_real2() * (double) pSet->iNumSolutes);  /* which solute is excluded? */
         if (iSol == pSet->iNumSolutes) iSol--;
         while (bSol[iSol] == false)
         {
            /* shift one to the right, if at end, cycle */
            iSol++;
            if (iSol == pSet->iNumSolutes) iSol = 0;
         }
         bSol[iSol] = false;
      }
   }
   else nSolutes = pSet->iNumSolutes;

   /* Finally: allocate arrays and init concentrations */
   //EndmConc   = (float *) memloc(pSet->iNumEndmembers * nSolutes * sizeof(float));
   EndmConc   = (float *) memloc(pSet->iNumEndmembers * pSet->iNumSolutes * sizeof(float));
   StreamConc = (float *) memloc(pSet->iNumSolutes * sizeof(float));
   Fracs  = (float *) memloc(nEndmembers * sizeof(float));
   EndmFracs  = (float *) memloc(pSet->iNumEndmembers * sizeof(float));
   Residuals  = (float *) memloc((pSet->iNumSolutes + 1) * sizeof(float));

   /* End member concentrations */
   for (iSol=0;iSol<pSet->iNumSolutes;iSol++)
   {
      for (iEM=0;iEM<pSet->iNumEndmembers;iEM++)
      {
         if (bSol[iSol] && bEM[iEM])
            EndmConc[iEM*pSet->iNumSolutes+iSol] = (float) EM[iEM]->aSolutes[iSol].get_value(mtrnd);
         else
            EndmConc[iEM*pSet->iNumSolutes+iSol] = pSet->nodata;
      }
   }
   /* End member fractions are always sampled */
   get_fractions(nEndmembers, Fracs, mtrnd);
   e = 0;
   for (iEM=0;iEM<pSet->iNumEndmembers;iEM++)
   {
      if (bEM[iEM])
      {
         EndmFracs[iEM] = Fracs[e];
         e++;
      }
      else
         EndmFracs[iEM] = pSet->nodata;
   }

   /* free mem */
   free(Fracs);
}

void rJob::ReadIter(int i,long *ChunkLong,int *ChunkInt,float *ChunkFloat)
{
   /* Recreate Job from chunk arrays */
   int nE = pSet->iNumEndmembers, nS = pSet->iNumSolutes;
   int bs = (nE+1)*nS+nE;

   /* job id */
   lJobID = ChunkLong[i];
   /* nendmembers and nsolutes */
   nEndmembers = ChunkInt[2*i];
   nSolutes = ChunkInt[2*i+1];
   
   /* Allocate arrays */
   EndmConc   = (float *) memloc(pSet->iNumEndmembers * pSet->iNumSolutes * sizeof(float));
   StreamConc = (float *) memloc(pSet->iNumSolutes * sizeof(float));
   EndmFracs  = (float *) memloc(pSet->iNumEndmembers * sizeof(float));
   Residuals  = (float *) memloc((pSet->iNumSolutes + 1) * sizeof(float));

   /* endmember concentrations, calculated stream concentrations, endmember fractions */
   memcpy(&EndmConc[0], &ChunkFloat[i*bs], nE*nS*sizeof(float));
   memcpy(&StreamConc[0],&ChunkFloat[i*bs+nE*nS], nS*sizeof(float));
   memcpy(&EndmFracs[0],&ChunkFloat[i*bs+nE*nS+nS], nE*sizeof(float));
}

void rJob::calc_stream(double *a, double *b, double *c)
{
   /* calculate stream concentrations based on end-member fractions and sampled end-member concentrations */
   int m, n, nrhs;
   double alpha, beta;

   /* size of matrices a and b depend on nEM and nSol for this iteration */
   m = nSolutes+1;
   n = nEndmembers;
   nrhs = 1;
   alpha = 1.;
   beta = 0.;

   /* matrix a and b: end member concentrations and fractions */
   fill_a(&a);
   fill_b(&b);
   /* calculate and store stream concentration */
   dgemm("N","N", &m, &nrhs, &n, &alpha, a, &m, b, &n, &beta, c, &m); /* BLAS-LAPACK matrix product */
   fill_stream(&c);
}

void rJob::llsapprox_fractions(int lwork, double *work, double *a, double *b, double *c, rStream *SW)
{
   /* approximate end-member fractions using linear least squares */
   int m, n, nrhs, info;
   double alpha, beta;

   /* size of matrices a and b depend on nEM and nSol for this iteration */
   m = nSolutes+1;
   n = nEndmembers;
   nrhs = 1;
   alpha = 1.;
   beta = 0.;

   if(pSet->bUseTempFile) infer_bools(); /* infer boolean bEM and bSol arrays for fill functions if from tempfile */

   fill_a(&a);  /* fill matrix a: end-member concentrations */
   fill_c(&b, SW);  /* fill matrix b with matrix c: stream concentrations, overwritten with end-member fractions by dgels */

   /* solve, exit(1) when no solution found */
   dgels( "No transpose", &m, &n, &nrhs, a, &m, b, &m, work, &lwork, &info ); /* BLAS-LAPACK linear least squares */
   if (info)
   {
      #pragma omp critical(ExitLLS)
      {
         printf("GEMMA FATAL ERROR: Least squares optimization returned no answer for job %i\nInfo: %i", lJobID, info);
         exit(1);
      }
   }

   /* calculate stream concentrations based on approximated end-member fractions */
   fill_a(&a);  /* refill matrix a: overwritten by dgels */
   dgemm("N","N", &m, &nrhs, &n, &alpha, a, &m, b, &n, &beta, c, &m); /* BLAS-LAPACK matrix product */

   /* store result: both end-member fracs and calculated stream conc */
   fill_fracs(&b);
   fill_stream(&c);
}

void rJob::infer_bools()
{
   /* if created from iter tempfile, bool arrays bEM and bSol are not set, but necessary before
      fill_a, fill_b, fill_c, fill_stream, fill_fracs */
   /* allocate bool end-member / solute arrays */
   bEM = (bool *) memloc(pSet->iNumEndmembers * sizeof(bool));
   bSol = (bool *) memloc(pSet->iNumSolutes * sizeof(bool));

   for (e=0;e<pSet->iNumEndmembers;e++)
   {
      if (EndmFracs[e] != pSet->nodata) bEM[e] = true;
      else bEM[e] = false;
   }
   for (s=0;s<pSet->iNumSolutes;s++) 
   {
      if (StreamConc[s] != pSet->nodata) bSol[s] = true;
      else bSol[s] = false;
   }
}
/* Fill functions, to go and from fullsize arrays including nodata (iNumEndmembers , iNumSolutes) to compressed arrays for BLAS routines */
void rJob::fill_a(double **a)
{
   /* fill matrix a using endmember concentrations
      mark unused parts of matrix a with -9999. */
   e = 0;
   for (iEM=0;iEM<pSet->iNumEndmembers;iEM++)
   {
      if (bEM[iEM])
      {
         (*a)[e*(nSolutes+1)] = pSet->dWbalFact * 1.;
         
         s = 0;
         for (iSol=0;iSol<pSet->iNumSolutes;iSol++)
         {
            if (bSol[iSol])
            {
               (*a)[1+e*(nSolutes+1)+s] = EndmConc[iEM*pSet->iNumSolutes+iSol];
               s++;
            }
         }
         e++;
      }
   }
   for (iSol=nEndmembers*(nSolutes+1);iSol<pSet->iNumEndmembers*(pSet->iNumSolutes+1);iSol++)
   {
      (*a)[iSol] = pSet->nodata;
   }
}

void rJob::fill_b(double **b)
{
   /* fill matrix b using end member fractions
      mark unused parts of matrix b with -9999.*/
   e = 0;
   for (iEM=0;iEM<pSet->iNumEndmembers;iEM++)
   {
      if (bEM[iEM])
      {
         (*b)[e] = EndmFracs[iEM];
         e++;
      }
   }
   for (iEM=nEndmembers;iEM<pSet->iNumEndmembers;iEM++)
   {
      (*b)[iEM] = pSet->nodata;
   }
}

void rJob::fill_c(double **c, rStream *SW)
{
   /* fill matrix c using streamwater concentrations
      mark unused parts of matrix c with -9999.*/
   (*c)[0] = pSet->dWbalFact * 1.;
   s = 0;
   for (iSol=0;iSol<nSolutes;iSol++)
   {
      if (bSol[iSol])
      {
         /*(*c)[1+s] = StreamConc[iSol];*/
         (*c)[1+s] = (float) SW->aSolutes[iSol].dMean;
         s++;
      }
   }
   for (iSol=nSolutes;iSol<pSet->iNumSolutes;iSol++)
   {
      (*c)[1+iSol] = pSet->nodata;
   }
}

void rJob::fill_stream(double **c)
{
   /* fill streamwater concentrations from dgemm result
      infer unused solutes from nodata values, as bool bSol array not set when from tempfile */
   fWbalCalc = (float) (*c)[0];
   s = 0;
   for (iSol=0;iSol<pSet->iNumSolutes;iSol++)
   {
      if (bSol[iSol])
      {
         StreamConc[iSol] = (float) (*c)[1+s];
         s++;
      }
      else
         StreamConc[iSol] = pSet->nodata;
   }
}

void rJob::fill_fracs(double **b)
{
   /* fill end-member fractions from dgels result
      ! dgels fills > nEndmember with rss */
   e = 0;
   for (iEM=0;iEM<pSet->iNumEndmembers;iEM++)
   {
      if (bEM[iEM])
      {
         EndmFracs[iEM] = (float) (*b)[e];
         e++;
      }
      else
         EndmFracs[iEM] = pSet->nodata;
   }
}

void rJob::calc_residuals(rStream *SW)
{
   /* Function to calculate residuals between calculated and measured stream concentrations */
   Residuals[0] = (float) (fWbalCalc - pSet->dWbalFact);
   for (int i=0;i<pSet->iNumSolutes;i++)
   {
      if (StreamConc[i] != pSet->nodata)
         Residuals[i+1] = (float) (StreamConc[i] - (float) SW->aSolutes[i].dMean);
      else
         Residuals[i+1] = pSet->nodata;
   }
}

float rJob::calc_likelihood(rStream *SW, int tid)
{
   /* Function to calculate behavioural / non-behavioural and likelihood
      criteria behavioural: all endmembers in [0-1], all stream solutes within ranges */
   bBehave = true;
   if (pSet->bUseLLSApprox)
   {
      for (iEM=0;iEM<pSet->iNumEndmembers;iEM++)  /* all endmembers between [0-1] */
      {
         if ((EndmFracs[iEM] != pSet->nodata) && (EndmFracs[iEM] < 0. || EndmFracs[iEM] > 1.))
         {
            bBehave = false;
            LH = 0.;
         }
      }
      if (abs(Residuals[0]) < pSet->dNormWbal) /* waterbalance: triangular LH. Always 1 if fractions are chosen to sum to 1 */
      {
         LH += (float) (1. - abs(Residuals[0]) / pSet->dNormWbal);
      }
      else
      {
         bBehave = false;
         LH = 0.;
      }
   }
   else LH = 1.0;  /* fractions sum exactly to 1 by design */

   /* fuzzy trapezoidal likelihood solutes: 1 when residual < nf1 * std, 0 - 1 when between nf1*std and nf2*std */
   for (iSol=0;iSol<pSet->iNumSolutes;iSol++)  
   {
      if (Residuals[1+iSol] != pSet->nodata)
      {
         /* if WithinTriangle: enforce solutes within fact2 */
         if (!pSet->bWithinTriangle || abs(Residuals[1+iSol]) <= pSet->dNormFact2 * SW->aSolutes[iSol].dStd)
         {
            LH += (float) (1. - std::max((abs(Residuals[1+iSol]) - pSet->dNormFact1 * SW->aSolutes[iSol].dStd) / 
                                        ((pSet->dNormFact2 - pSet->dNormFact1) * SW->aSolutes[iSol].dStd), 0.0));
         }
         else
         {
            bBehave = false;
            LH = 0.;
            break;
         }
      }
   }
   LH /= (nSolutes+1);
   if (LH <= 0)
   {
      bBehave = false;
      LH = 0;
   }
   return LH;
}

void rJob::fill_chunk(int i, long **ChunkLong, int **ChunkInt, float **ChunkFloat)
{
   /* Fill chunk array with Job values */
   int nE = pSet->iNumEndmembers;
   int nS = pSet->iNumSolutes;
   int bs = (nE+1)*nS+nE;
   /* fill chunk with Job values */
   (*ChunkLong)[i] = lJobID;
   (*ChunkInt)[2*i] = nEndmembers;
   (*ChunkInt)[2*i+1] = nSolutes;
   memcpy(&(*ChunkFloat)[i*bs],&EndmConc[0], nE*nS*sizeof(float));
   memcpy(&(*ChunkFloat)[i*bs+nE*nS],&StreamConc[0], nS*sizeof(float));
   memcpy(&(*ChunkFloat)[i*bs+nE*nS+nS],&EndmFracs[0], nE*sizeof(float));
}

void rJob::write(FILE *fOut)
{
   /* Write values to binary output file */
   /* job id */
   ra_write(fOut, &lJobID, 1, sizeof(long));
   /* first: nendmembers+which and nsolutes+which */
   ra_write(fOut, &nEndmembers);
   /*ra_write(fOut, bEM, pSet->iNumEndmembers, sizeof(bool));*/
   ra_write(fOut, &nSolutes);
   /*ra_write(fOut, bSol, pSet->iNumSolutes, sizeof(bool));*/
   /* endmember concentrations */
   ra_write(fOut, EndmConc, pSet->iNumEndmembers*pSet->iNumSolutes, sizeof(float));
   /* streamwater concentrations */
   ra_write(fOut, StreamConc, pSet->iNumSolutes, sizeof(float));
   /* endmember fractions */
   ra_write(fOut, EndmFracs, pSet->iNumEndmembers, sizeof(float));
   //if (!bInit)
   //{
      /* residuals, only when diagnostics */
      if (pSet->bDiag) ra_write(fOut, Residuals, pSet->iNumSolutes+1, sizeof(float));
      /* LH */
      ra_write(fOut, &LH, 1, sizeof(float));
      /* Behavioural, only when diagnostics */
      if (pSet->bDiag) ra_write(fOut, &bBehave, 1, sizeof(bool));
   //}
}