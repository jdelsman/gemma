/****************************************************************
 *                                                              *
 *  GEMMA - End-member mixing in a GLUE framework               *
 *  Joost Delsman, Deltares / VU Amsterdam, 2012-2013           *
 *                                                              *
 *  Mersenne-Twister algorithm used in Monte-Carlo              *
 *  by Takuji Nishimura and Makoto Matsumoto. (see mtrnd_mt.h)  *
 *                                                              *
 ****************************************************************

Copyright (c) 2013, Deltares / VU Amsterdam
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef GEMMA_H
#define GEMMA_H

#include "mtrnd_mt.h"

#include "math.h"
//#include "gemma_read.h"
//#include "gemma_helperfunc.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>
//#include <random>
#include <algorithm>

#define MAXLENGTH 512
#define CHUNKSIZE 1000//0000

/* class definitions */
class rSettings
{
public:
   bool           bEndmembersRandom;         /* random pick of endmembers from given endmembers? (default: no) */
   bool           bSolutesRandom;            /* random pick of solutes from given solutes? (default: no) */
   bool           bDiag;                     /* write diagnostics to output file? */
   bool           bUseLLSApprox;             /* use least squares approximation to calculate end member fractions (default: no) */
   bool           bWithinTriangle;           /* enforce all solutes within likelihood triangle instead of average (default: no) */
   bool           bStopMaxBehave;            /* stop iteration when max behavioural is reached, set nbehave to 'projected' value (default: no) */
   bool           bUseTempFile;              /* use temporary iterations file (default: yes) */
   bool           bUseExistingTempFile;      /* use existing temporary iterations file (default: no) */
   int            iNumEndmembers;            /* number of different endmembers (default: 0) */
   int            iMinEndmembers;            /* minimal number of different endmembers if random (default: 1) */
   int            iNumThreads;               /* number of threads to be used (default: system max) */
   int            iNumSolutes;               /* number of solutes */
   int            iMinSolutes;               /* minimal number of solutes if random (default: 1, but enforced to always exceed nr of endmembers) */
   int            iNumStreamSamples;         /* number of stream samples */
   long           lNumIter;                  /* number of iterations (default: 100000) */
   long           lMaxBehavioural;           /* maximum stored behavioural runs (default: all (-1))*/
   long           lMaxDiag;                  /* maximum number of rows to write to output file in diagnostics mode */
   long           lMaxSave;                  /* maximum number of jobs to save */
   float          nodata;                    /* nodata value (-9999.)*/
   double         dNormRSS;                  /* is RSS < norm: behavioural */
   double         dNormFact1;                /* is residual < normfact1 * std: likelihood = 1 (default: 1) */
   double         dNormFact2;                /* is residual < normfact2 * std: likelihood > 0 (default: 3) */
   double         dNormWbal;                 /* maximum residual waterbalance (default: 0.05) */
   double         dWbalFact;                 /* relative importance of water balance in equation (default: 1) */
   char         **acSoluteNames;             /* array of solute names */
   char           fnSettings[MAXLENGTH];     /* filename settings file */
   char           fnEndmembers[MAXLENGTH];   /* filename endmembers file */
   char           fnStream[MAXLENGTH];       /* filename stream file */
   char           fnOutput[MAXLENGTH];       /* filename binary output file */
   char           fnIter[MAXLENGTH];         /* filename temporary binary iterations file */
   size_t         blocksize;                 /* blocksize in temporary binary iterations file */
   rSettings() 
   { 
      /* set defaults (-1: set in read_ini() )*/
      lNumIter  = 100000; 
      bDiag = false;
      bEndmembersRandom = false;
      bSolutesRandom = false;
      bUseLLSApprox = false;
      bWithinTriangle = false;
      bStopMaxBehave = false;
      bUseTempFile = true;
      bUseExistingTempFile = false;
      iNumEndmembers = 1;
      iMinEndmembers = 1;
      iNumThreads = omp_get_max_threads();
      iNumSolutes = 1;
      iMinSolutes = 1;
      iNumStreamSamples = 0;
      lMaxBehavioural = -1;
      dNormRSS = 0;
      dNormFact1 = 1.0;
      dNormFact2 = 3.0;
      dNormWbal = 0.05;
      dWbalFact = 1.0;
      acSoluteNames = NULL;
      strcpy(fnSettings, "gemma.ini");
      strcpy(fnEndmembers, "gemma_endmembers.txt");
      strcpy(fnStream, "gemma_stream.txt");
      strcpy(fnOutput, "gemma_output.bin");
      strcpy(fnIter, "gemma_temp.iter");
      lMaxDiag = -1;
      nodata = -9999.;
      blocksize = 0;
   }
};

class rSolute
{
private:
   double nvmc;
public:
   char           acSolute[15];  /* code of solute */
   char           cDist;         /* code of distribution function (uniform: U, gaussian: G)*/
   double         dMean;         /* mean value (gaussian distribution) */
   double         dStd;          /* standard deviation (gaussian distribution) */
   double         dMin;          /* minimum value (uniform distribution) */
   double         dMax;          /* maximum value (uniform distribution) */
   bool           bRatio;        /* is a ratio of tracers? */
   rSolute() 
   { 
      nvmc  = 4.0 * exp(-0.5)/ sqrt(2.0); 
      cDist = 'G';
      dMean = 0.0;
      dStd = 0.0;
      dMin = 0.0;
      dMax = 0.0;
      bRatio = false;
   }

public:
   double get_value(MT::MersenneTwist *mtrnd);
};

class rSample
{
public:
   int      nSolutes;               /* number of solutes */
   char     **acSoluteNames;        /* array of solute names */
   rSolute  *aSolutes;              /* array of solute params */

   rSample(rSettings *pSet=NULL)
   {
      if (pSet != NULL)
      {
         /* create array of solute names and
            reserve memory for array of UncConc per solute */
         nSolutes = pSet->iNumSolutes;
         acSoluteNames = new char *[nSolutes];
         aSolutes = new rSolute[nSolutes];
         for (int i=0;i<nSolutes;i++)
         {  
            acSoluteNames[i] = new char[strlen(pSet->acSoluteNames[i])+1];
            aSolutes[i] = rSolute();
            strcpy(acSoluteNames[i], pSet->acSoluteNames[i]);
            strcpy(aSolutes[i].acSolute, acSoluteNames[i]);
         }
      }
   }
   ~rSample() 
   { 
      for (int i=0;i<nSolutes;i++)
      {
         delete[] acSoluteNames[i];
      }
      delete[] acSoluteNames, aSolutes;
   }
   int get_index(const char *Solute) const;
};

class rEndmember : public rSample
{
public:
   char     *acName;      /* Name of endmember */

   rEndmember(char *Name=NULL, rSettings *pSet=NULL) : rSample(pSet)
   {
      if (Name != NULL)
      {
         acName = new char[strlen(Name)+1];
         strcpy(acName, Name);
      }
   }
};

class rStream : public rSample
{
public:
   char     *acName;      /* Name of stream sample */
   char     *acDatetime;  /* Datetime (isoformat, YYYY-MM-DDThh:mm:ss) of stream sample */

   rStream(char *Name=NULL, char *Datetime=NULL, rSettings *pSet=NULL) : rSample(pSet)
   {
      if (Name != NULL)
      {
         acName = new char[strlen(Name)+1];
         strcpy(acName, Name);
         acDatetime = new char[strlen(Datetime)+1];
         strcpy(acDatetime, Datetime);
      }
   }
};

class rJob
{
   int        e, s, iEM, iSol;
   bool       bInit;         /* init from scratch? */
public:
   long       lJobID;        /* job identification */
   int        nEndmembers;   /* number of endmembers in job */
   int        nSolutes;      /* number of solutes in job */
   bool      *bEM;           /* endmember present in job or not */
   bool      *bSol;          /* solute present in job or not */
   bool       bBehave;       /* this job behavioural? */
   float      LH;            /* likelihood for job */
   float     *EndmConc;      /* concentrations for endmembers in job (size: nEndm (major) * nSol) */
   float     *StreamConc;    /* streamwater concentrations in job (size: nSol) */
   float     *EndmFracs;     /* calculated fractions of endmembers in job (size: nEndm) */
   float     *Residuals;     /* calculated residuals of concentrations in job (size: nSol + 1) */
   float      fWbalCalc;     /* calculated waterbalance (if LLS) */
   rSettings *pSet;          /* pointer to settings class */

   rJob(bool bI, long lID, rSettings *pS, rEndmember **EM, MT::MersenneTwist *mtrnd, int i=0,long *ChunkLong=NULL,int *ChunkInt=NULL,float *ChunkFloat=NULL);
   ~rJob() 
   { 
      /* Destructor rJob:
         - deallocate arrays */
      free(EndmConc);
      free(StreamConc);
      free(EndmFracs);
	  free(Residuals);
      if (bInit)
      {
         free(bEM);
         free(bSol);
      }
   }
   void InitIter(long lID, rEndmember **EM, MT::MersenneTwist *mtrnd);
   void ReadIter(int i,long *ChunkLong,int *ChunkInt,float *ChunkFloat);
   void calc_stream(double *a, double *b, double *c);
   void llsapprox_fractions(int lwork, double *work, double *a, double *b, double *c, rStream *SW);
   void infer_bools();
   void fill_a(double **a);
   void fill_b(double **b);
   void fill_c(double **c, rStream *SW);
   void fill_stream(double **c);
   void fill_fracs(double **b);
   void calc_residuals(rStream *SW);
   float calc_likelihood(rStream *SW, int tid);
   void fill_chunk(int i,long **ChunkLong,int **ChunkInt,float **ChunkFloat);
   void write(FILE *fOut);
};


/* read functions in gemma_read.h */
extern void read_ini(rSettings *);
extern rEndmember **read_endmembers(rSettings *);
extern rStream **read_streamwater(rSettings *);
extern FILE *open_output(rSettings *, rEndmember **);
extern FILE *open_iter(rSettings *);

#endif // GEMMA_H