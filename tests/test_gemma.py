import pytest
import matplotlib.pyplot as plt

import gemma


def test_gemma():
    g = gemma.gemma(
        "test_data/gemma_output.bin", endian="<"
    )  # force endianness to Windows

    assert g.endmembers == ["GWPH", "GWDB", "GWAQD", "INLET", "PRECIP"]

    assert g.nsamples == 50

    s = g[49]
    assert s.name == "gh-050"

    s.read_data()
    assert round(s.data.iloc[0]["GWPH_Cl"], 3) == 90.843
