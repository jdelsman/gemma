import gemma
import gemma.plotting as gp
import matplotlib.pyplot as plt

fn_output = "gemma_output.bin"

# open g-emma output file
g = gemma.gemma(fn_output)

print ("Endmembers: %s" % ", ".join(g.endmembers))
print ("Samples:    %i" % g.nsamples)
print ("Iterations: %i" % g.niterations)

# loop through samples and print name and n of behaviorals
print "\nSamples:"
for s in g:
    print ("Sample: %s, %i behaviorals" % (s.name, s.nbehave))

# save file with endmember fractions for the median and 25-75 percentile range
g.percentiles([0.25, 0.5, 0.75], g.endmembers).to_csv("em_fractions.csv")

# plot violin plot and dotty plot of last sample
plt.ioff()
s.boxplot(g.endmembers)
plt.show()
s.dottyplot(g.endmembers)
plt.show()

# plot em fractions plot
gp.endmembers_time((25, 50, 75), result=g)
plt.show()
