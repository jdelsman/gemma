G-EMMA Features
===============

* Can operate with both linear least squares approximation of end-member fractions, and sampling of end-member fractions from a Dirichlet distribution. Standardization of concentrations to zero mean and standard deviation of one is advised when using lls approximation.
* Parallel processing: G-EMMA supports parallel processing on multi-core machines through use of the `OpenMP library <http://openmp.org>`__. G-EMMA by default uses the maximum number of cores available on a machine, but this behaviour can be manipulated through the nthreads parameter in the ini-file.
* Adjustable re-use of iteration data between separate samples (speeding up calculation times), file storage permitting.
* Possibility to randomize both used end-members and solutes in end-member mixing calculations.
* Possibility of using either uniform or normal end-member concentration distributions.
* In Linear-Least-Squares approximation: flexible weighting of water balance closure.
* Flexibility in defining fuzzy likelihood measures.