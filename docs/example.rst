G-EMMA example code
===================

The following is an example of how to use the G-EMMA python module
to obtain the results from the binary G-EMMA output file.


.. literalinclude:: example_gemma.py
  :language: python