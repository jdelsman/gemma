gemma  -  G-EMMA results
------------------------

.. automodule:: gemma
    :members:
    :imported-members:
    :undoc-members:
    :show-inheritance:
