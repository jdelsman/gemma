G-EMMA File descriptions
########################

G-EMMA requires three files to operate:

* :ref:`Initialization or settings file<Initialization file>`
* :ref:`End-member concentrations file`
* :ref:`Stream samples file`

Simulation results are outputted in a :ref:`binary output file<Binary output file>`. This binary file can be read using the python gemma module.

Initialization file
*******************

The default name of the G-EMMA initialization file is gemma.ini, located in the program's working directory. Alternative 
filenames or file locations may be entered as the first argument when running the executable.

The G-EMMA initialization file may contain the option keywords listed below. If an option is missing from the file, the default 
value is used. Keywords and their value must be separated by a ``<tab>``. Comments must be marked with an ``!`` as the first character of 
a line. In the table below, bold marks the minimum necessary part of a keyword or value to be recognized. Keywords and values are 
case-insensitive.

.. list-table:: Options in initialization file
   :widths: 30 40 30
   :header-rows: 1

   * - Option
     - Description
     - Default value
   * - **fnend**\ members
     - Filename of end-members file
     - gemma_endmembers.txt
   * - **fnstr**\ eam
     - Filename of stream samples file
     - gemma_stream.txt
   * - **fnout**\ put
     - Filename of (binary) output file
     - gemma_output.bin
   * - **fniter**\ ations
     - Filename of (optional) temporary iterations file
     - gemma_temp.iter
   * - **usetemp**\ file
     - Use temporary iterations file (True / False)
     - True
   * - **useexis**\ tingtemp
     - If available, use existing temporary iterations file (from previous calculation) (T/F)
     - False
   * - **niter**\ ations
     - Number of iterations (enforced to be a multiple of 1e5)
     - 1e5
   * - **nthre**\ ads
     - Number of threads / cpus to use for the calculation
     - System maximum
   * - **nendm**\ embers
     - Number of end-members
     - 
   * - **randome**\ ndmembers
     - Choose random set of end-members from full set
     - False
   * - **minen**\ dmembers
     - Minimum number of end-members in a random set
     - nendmembers
   * - **nsolu**\ tes
     - Number of solutes used as tracer
     - 
   * - **solut**\ es
     - Solutes to use as tracer, input as tab-separated list
     - 
   * - **randoms**\ olutes
     - Choose random set of solutes as tracers from full set
     - False
   * - **minso**\ lutes
     - Minimum number of solutes in a random set
     - nsolutes
   * - **withi**\ ntrapezoid
     - Enforce all solutes within their respective fuzzy trapezoid (T/F). The alternative is to allow negative fuzzy membership for solutes, which can average out over all solutes (not recommended)
     - True
   * - **normfact1**
     - Fuzzy trapezoid per solute is 1 if residual is within range normfact1*(-anal. std. - anal. std.)
     - 1.
   * - **normfact2**
     - Fuzzy trapezoid per solute is >0  if residual is within range normfact2*(-anal. std. - anal. std.)
     - 3.
   * - **wbalf**\ actor
     - Factor to influence the relative importance of closing the water balance. Only applicable when LLS approximation is used.
     - 1.
   * - **wbaln**\ orm
     - Fuzzy triangle of water balance is > 0 if residual is within (-wbalnorm - wbalnorm). Only applicable when LLS approximation is used.
     - 0.05
   * - **usells**\ approximation
     - Obtain end-member fractions by least squares approximation instead of sampling (T/F)
     - False
   * - **stop**\ maxbehave
     - Stop iterating when the maximum amount of saved behavioural results is reached. The total number of behavioural runs is determined by extrapolating the behavioural / total tries ratio (T/F)
     - False
   * - **maxbe**\ have
     - Maximum amount of behavioural runs to save to output file
     - niterations
   * - **diagn**\ ostics
     - Operate in diagnostics mode: also output non-behavioural runs to output file (T/F)
     - False
   * - **maxdi**\ agnostics
     - Maximum amount of runs to save to output file in diagnostics mode
     - niterations


Example of a G-EMMA initialization file :

.. literalinclude:: gemma.ini


End-member concentrations file
******************************

The end-members concentrations input file contains information on the concentration ranges of tracers for the various end-members. 
In addition, this file determines which end-members are used in the analysis (together with the nendmembers keyword in the initialization 
file). Comment lines start with an ``!``.

Blocks of end-member concentrations are specified by a bracketed end-member code on the first line: ``[end-member]``.

On the following lines, for each solute a tab-separated list must be entered containing: 
solute - distribution code - minimum value - maximum value - mean value - standard deviation. Order of the solutes is not important. 
G-EMMA only supports distributions ``U`` (uniform) and ``G`` (gaussian). The former uses the minimum - maximum values, while the latter uses the 
mean and standard deviation.

Only the first ``nendmembers`` end-members are used in the simulation.

Example of a end-members input file:

.. literalinclude:: gemma_endmembers.txt


Stream samples file
*******************

The stream samples file contains the measured concentration and analytical uncertainty of all tracers for all stream samples. The file starts with 
an integer value signaling the amount of samples in the file. The next line is the header, and must contain Sample - Datetime as the first two columns 
(tab-separated). Further columns contain each solute and the analytical standard deviation of each solute. All further lines contain the corresponding 
tab-separated values for the samples. Format of the datetime column is unicode format: ``yyyy-mm-ddThh:mm:ss`` (not necessary for G-EMMA, but read by the 
Python module). Comment lines start with an ``!``.

Example of a stream samples file:

.. literalinclude:: gemma_stream.txt


Binary output file
******************

The output file is a binary file, containing the behavioural simulations per sample (or all if in diagnostics mode). The output file can be read by the python
gemma module. 

The structure of the file is as follows:

File header
-----------

.. list-table:: File header
   :widths: 30 70
   :header-rows: 1

   * - Type
     - Description
   * - int
     - number of end-members (nE)
   * - nE\*string
     - each end-member name as a string\*
   * - int
     - number of solutes (nS)
   * - nS\*string
     - each solute name as a string\*
   * - bool
     - diagnostics mode?
   * - bool
     - using Linear Least Squares approximation?
   * - bool
     - enforce behaviourals within trapezoids?
   * - bool
     - stop at max saved behaviourals?
   * - bool
     - write shorter file <deprecated>

\*strings are saved as follows:

.. list-table:: String format
   :widths: 30 70
   :header-rows: 1

   * - Type
     - Description
   * - int
     - number of characters in string
   * - n*char
     - string

Sample results
--------------

.. note::
   Note that samples are not necessarily stored in the same order as in the stream samples file, due to the parallel processing of samples.

**Sample header**

.. list-table:: Sample header
   :widths: 30 70
   :header-rows: 1

   * - Type
     - Description
   * - string\*
     - sample name
   * - string\*
     - datetime
   * - long
     - number of behavioural simulations
   * - long
     - number of simulations saved

**Sample data**

For each behavioural iteration, or all when in diagnostics mode:

.. list-table:: Sample data
   :widths: 30 70
   :header-rows: 1

   * - Type
     - Description
   * - long
     - Iteration number
   * - int
     - Number of end-members in this iteration
   * - int
     - Number of solutes in this iteration
   * - nE\*nS\*float
     - End-member concentrations in this iteration
   * - nS\*float
     - Calculated stream concentration in this iteration
   * - nE\*float
     - Calculated end-member fractions in this iteration
   * - (nS+1)\*float
     - Residuals in this iteration (only when in diagnostics mode)
   * - float
     - Likelihood of iteration
   * - bool
     - Behavioural? (only when in diagnostics mode)

