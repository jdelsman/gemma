.. imod documentation master file, created by
   sphinx-quickstart on Tue Apr 10 12:38:06 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

G-EMMA: end-member mixing analysis within a GLUE uncertainty assessment framework
=================================================================================

Using mixing model approaches to separate the different components of a hydrograph has a long and successful 
history in the hydrological community. However, the application of mixing models suffers from uncertainty in 
both the identification of the correct sources (end-members) and the unavoidable spatiotemporal variation in 
end-member concentrations. Before G-EMMA, no method accounted for both these sources of uncertainty.

.. raw:: html

   <br/>
      <img src="_static/logo_banner.png"/>


G-EMMA is a novel method of uncertainty assessment in end-member mixing analysis, based on generalized likelihood 
uncertainty estimation, and has been successfully applied to a lowland polder catchment, part of the Haarlemmermeer. 
This catchment provided for a difficult test case, due to a large spatial variability in end-member concentrations, 
and a large number of different distinguishable water types. G-EMMA was better able to deal with the large number of 
and spatial variation in end-members than the traditional approach, suggesting possible application over a wider range 
of catchments than traditional EMMA.

The G-EMMA procedure and its application to a Dutch polder catchment have been published in a scientific paper 
(Delsman et al., 2013).

Source code and Windows binaries of G-EMMA can be found at https://gitlab.com/jdelsman/gemma.

Published applications of G-EMMA
--------------------------------
* Delsman, J.R., Oude Essink, G.H.P., Beven, K.J., Stuyfzand, P.J., 2013. Uncertainty estimation of end-member mixing using generalized likelihood uncertainty estimation (GLUE), applied in a lowland catchment. `Water Resour. Res. 49, 4792-4806 <http://doi.org/10.1002/wrcr.20341>`__
* Koutsouris, A.J., Lyon, S.W., 2018. Advancing understanding in data-limited conditions: estimating contributions to streamflow across Tanzania s rapidly developing Kilombero Valley. `Hydrol. Sci. J. 63, 197-209 <http://doi.org/10.1080/02626667.2018.1426857>`__.


Documentation
-------------

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: G-EMMA

   Introduction <self>
   

**G-EMMA executable**

* :doc:`features`
* :doc:`filedescriptions`

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: G-EMMA executable

   features
   filedescriptions

**G-EMMA python module**

* :doc:`example`
* :doc:`api`
* :doc:`genindex`

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Python API Reference

   example
   api
   genindex


Installing
----------

The `gemma` Python package can be installed after downloading all the files from
the repository, or after cloning the repository using git:

.. code-block:: console

  git clone https://gitlab.com/jdelsman/gemma.git
  
To install the gemma python module:

.. code-block:: console

  cd gemma
  pip install -e .


