API Reference G-EMMA python module
==================================

.. toctree::
    :maxdepth: 2
    :caption: API Reference

    api/gemma.rst
    api/plotting.rst
    api/gemmafiles.rst
