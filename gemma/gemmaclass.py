from .readfuncs import *
from .sampleclass import sample
import os
import numpy as np
import pandas


class gemma(object):
    """ Class to read out gemma output.bin

        Reads in header information and further acts as an iterator object. Data from samples are not read by default.
        
        Samples can be accessed as gemma[samplename] or gemma[sampleindex]

        Parameters
        ----------
        fname : str, filename of binary gemma output file, default gemma_output.bin
        version : version of gemma executable, default 34
        persistent : keep samples in memory if True (default), otherwise re-read from file

        Attributes
        ----------
        fname : str
            filename
        nsamples : int
            number of samples
        niterations : int
            number of iterations
        diag : bool
            g-emma diagnostics mode
        useLLS : bool
            linear least squares approximation used in g-emma
        withintriangle : bool
            behaviourals constrained within individual trapezoids/triangles
        stopmax : bool
            stopped iterations when maximum behaviourals were reached (and nbehave is therefore an approximation)
        nendmembers : int
            number of end-members
        endmembers : list of strings
            end-member names
        nsolutes : int
            number of solutes
        solutes : list of strings
            solute names
        samples : dict
            sample objects, with samplenames as keys
        samplenames : list
            sample names
        persistent : bool
            keep samples in memory
        version : int
            version of G-EMMA executable
        
        """

    def __init__(
        self, fname="gemma_output.bin", version=34, persistent=True, endian="@"
    ):
        self.data_pos = 0
        self.cur_pos = 0
        self.cur_s = 0
        self.fname = fname
        self.nsamples = 0
        self.niterations = 0
        self.nendmembers = 0
        self.diag = False
        self.useLLS = False
        self.withintriangle = False
        self.stopmax = False
        self.short = False
        self.endmembers = []
        self.nsolutes = 0
        self.solutes = []
        self.columns = ["ITER", "NENDM", "NSOL"]
        self.samples = {}
        self.samplenames = []
        self.samplepos = {}
        self.readsamples = False
        self.persistent = persistent  # keep samples in memory
        self.version = version  # version of G-EMMA executable
        self.endian = endian
        try:
            self.f = open(self.fname, "rb")
        except IOError:
            raise IOError("IOError reading %s" % self.fname)
        self._read_header()
        iter(self)

    def _read_header(self):
        self.f.seek(0, os.SEEK_SET)
        self.x64 = readbool(self.f, endian=self.endian)
        self.nsamples = readint(self.f, endian=self.endian)
        self.niterations = readlong(self.f, endian=self.endian)
        self.nendmembers = readint(self.f, endian=self.endian)
        for i in range(self.nendmembers):
            self.endmembers += [readstring(self.f, endian=self.endian)]
        self.nsolutes = readint(self.f, endian=self.endian)
        for i in range(self.nsolutes):
            self.solutes += [readstring(self.f, endian=self.endian)]
        self.diag = readbool(self.f, endian=self.endian)
        if self.version > 33:
            self.useLLS = readbool(self.f, endian=self.endian)
            self.withintriangle = readbool(self.f, endian=self.endian)
            self.stopmax = readbool(self.f, endian=self.endian)
        self.short = readbool(self.f, endian=self.endian)

        # create columns for dataframe
        for e in self.endmembers + ["MIX"]:  # solute concentrations
            for s in self.solutes:
                self.columns += ["%s_%s" % (e, s)]
        for e in self.endmembers:  # fractions
            self.columns += [e]
        if self.diag:
            self.columns += ["Res_WB"]
            for s in self.solutes:
                self.columns += ["Res_%s" % s]

        if self.version != 52:
            self.columns += ["LH"]
        if self.diag:
            self.columns += ["Behavioural"]

        # store position of cursor at beginning of data
        self.data_pos = self.f.tell()

    def _read_all(self):
        """From v39: samples no longer in order,
           order sample list based on sample dates
           Function must be called before initialisation of iterator"""
        i = 1
        sampledates = []
        self.cur_pos = self.data_pos
        while 1:
            self.f.seek(self.cur_pos)
            try:
                s = sample(self, False)
                self.cur_pos = self.f.tell()
                self.samplenames += [s.name]
                sampledates += [s.date]
                self.samples[s.name] = s
                i += 1
            except (IOError, struct.error):
                # raise IOError('error at sample %i'%i)
                break
        self.samplenames = [x for (y, x) in sorted(zip(sampledates, self.samplenames))]
        self.readsamples = True

    def __iter__(self):
        if not self.readsamples:
            self._read_all()
        self.cur_pos = self.data_pos
        self.cur_s = -1
        return self

    def __next__(self):
        """Iterator over samples. Only through ordered samples list/dict"""
        self.cur_s += 1
        if self.cur_s < len(self.samples):
            return self.samples[self.samplenames[self.cur_s]]
        else:
            raise StopIteration

    next = __next__  # for Python 2

    def read_data(self, nrows=None):
        """ Read data from samples. Not done by default

        Parameters
        ----------
        nrows : int, no of rows to read from sample data. Use None (default) for all
        """
        for s in self:
            s.read_data(nrows)

    def print_summary(self, ret=False):
        """Print summary data on gemma run

        Parameters
        ----------
        ret : return nb (False)

        Returns
        -------
        nb : pandas Series of no of behavioural simulations for each sample
        """
        nb = []
        for s in self:
            nb += [s.nbehave]
        nb = pandas.Series(nb)
        s = "G-EMMA run\n-----------------\n"
        s += "%i samples, %i yielded behavioural runs\n" % (
            self.nsamples,
            len(nb.nonzero()[0]),
        )
        # s += '%.2e behavioural (s=%.2e) of %.2e runs\n'%(nb.mean(),nb.std(),
        #                                                 self.niterations)
        s += "Distribution of behaviourals (out of %.2e runs):" % self.niterations
        print(s)
        print(nb.describe())
        if ret:
            return nb

    def read_priors(self, fname="gemma_temp.prior"):
        """Read PDFs and CDFs for endmember - solute combinations per sample"""
        pdf = []
        cdf = []
        ranges = {}
        idxpdf = {}
        idxcdf = []
        idxsamples = ["PRIOR"] + self.samplenames

        with open(fname, "rb") as f:
            iN = readint(f)
            idxcdf = np.linspace(0, 1.0, iN + 1)
            idxcdf = ["%f-%f" % (f1, f2) for f1, f2 in zip(idxcdf[:-1], idxcdf[1:])]

            for e in self.endmembers:
                for s in self.solutes:
                    emsol = e + "_" + s
                    ranges[emsol] = (readdouble(f), readdouble(f))
                    idxpdf[emsol] = np.linspace(
                        ranges[emsol][0], ranges[emsol][1], iN + 1
                    )
                    idxpdf[emsol] = [
                        "%f-%f" % (f1, f2)
                        for f1, f2 in zip(idxpdf[emsol][:-1], idxpdf[emsol][1:])
                    ]

            # get priors
            for i, sample in enumerate(idxsamples):
                try:
                    this_pdf = []
                    this_cdf = []
                    for e in self.endmembers:
                        for s in self.solutes:
                            emsol = e + "_" + s
                            this_pdf += [
                                pandas.DataFrame(
                                    readarray(f, 1, iN, "d"),
                                    [[emsol] * iN, idxpdf[emsol]],
                                    columns=[sample],
                                )
                            ]
                            this_cdf += [
                                pandas.DataFrame(
                                    readarray(f, 1, iN, "d"),
                                    [[emsol] * iN, idxcdf],
                                    columns=[sample],
                                )
                            ]
                        pdf += [pandas.concat(this_pdf)]
                        cdf += [pandas.concat(this_cdf)]
                except:
                    # raise
                    # print 'error'
                    # return pdf, cdf
                    pass

        # return pdf, cdf
        return pandas.concat(pdf, axis=1), pandas.concat(cdf, axis=1)

    def em_contrib(self):
        """Calculate end-member contributions for all samples, constructs DataFrame

        Returns
        -------
        emc : pandas.DataFrame with end-member contributions
        """
        emc = []
        nms = []
        dts = []
        for s in self:
            if s.nbehave:
                emc.append(s.em_contrib())
                nms.append(s.name)
                dts.append(s.date)
        # return ((pandas.concat(emc, keys=dts, axis=1)).T).fillna(0)
        df = ((pandas.concat(emc, axis=1)).T).fillna(0)
        df = df.reset_index().rename(columns={"index": "Sample"})
        df.loc[:, "Date"] = dts

        return df

    def percentiles(self, percentiles, params, mask=None):
        """Create DataFrame with percentiles of parameters for all samples.
        For subsequent use sets self.pctresult with the result. Uses self.pctresult if requested parameters / percentiles are in self.pctresult

        Parameters
        ----------
        percentiles : list-like of floats. Percentiles in the 0-100 range are reset to the 0-1 range.
        params : list of parameters to calculate percentiles of.
        mask : tuple with first item parameter name and second item equality

        Returns
        -------
        pctresult : pandas.DataFrame
        """
        if percentiles[-1] > 1:
            percentiles = [x / 100.0 for x in percentiles]
        ids = []
        dates = []
        data = []

        prms_new = []
        if "pctresult" in dir(self):
            # check if some percentages / params already exist
            for p in params:
                skip = False
                for pct in percentiles:
                    if (p, pct) not in self.pctresult:
                        skip = True
                if skip:
                    prms_new += [p]
            # params = prms_new
        else:
            prms_new = params

        # still params to do?
        if len(prms_new):
            for s in self:
                if s.nbehave:
                    if not s.has_data:
                        s.read_data()

                    ids += [s.name]
                    dates += [s.date]
                    if isinstance(mask, (tuple, list)):
                        mask = s.data[mask[0]] == mask[1]
                        data += [
                            s.percentile(
                                pct=percentiles, params=prms_new, mask=mask
                            ).T.stack()
                        ]
                    else:
                        data += [
                            s.percentile(pct=percentiles, params=prms_new).T.stack()
                        ]

            A = pandas.concat(data, keys=ids, axis=1).T
            A["Date"] = dates
            A["Sample"] = ids

            if "pctresult" in dir(self):
                try:
                    for c in A.columns:
                        if c in self.pctresult.columns:
                            C = A.pop(c)
                    self.pctresult = pandas.merge(
                        self.pctresult,
                        A,
                        how="outer",
                        left_index=True,
                        right_index=True,
                    )
                except:
                    print(self.pctresult, A)
                    raise
            else:
                self.pctresult = A

        # only return asked parameters...
        cols = [("Date", ""), ("Sample", "")] + [
            (p, pct) for p in params for pct in percentiles
        ]
        return self.pctresult[cols]
        # return self.pctresult

    # overload []
    def __getitem__(self, index):
        if isinstance(index, int):
            return self.samples[self.samplenames[index]]
        else:
            return self.samples[index]

    def __del__(self):
        self.f.close()


if __name__ == "__main__":
    g = gemma()
