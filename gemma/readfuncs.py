import struct
import numpy as np


def readbool(f, endian="@"):
    return struct.unpack("{}?".format(endian), f.read(1))[0]


def readint(f, endian="@"):
    return struct.unpack("{}i".format(endian), f.read(4))[0]


def readlong(f, endian="@"):
    return struct.unpack("{}l".format(endian), f.read(4))[0]


def readfloat(f, endian="@"):
    return struct.unpack("{}f".format(endian), f.read(4))[0]


def readdouble(f, endian="@"):
    return struct.unpack("{}d".format(endian), f.read(8))[0]


def readstring(f, endian="@"):
    l = readint(f)
    return struct.unpack("{}{:d}s".format(endian, l), f.read(l))[0].decode("UTF-8")


def readarray(f, m, n, dtype="f", endian="@"):
    if dtype == "f":
        tsize = 4
    elif dtype == "d":
        tsize = 8

    if m == 1:
        return np.array(
            struct.unpack("{}{:d}{}".format(endian, n, dtype), f.read(n * tsize))
        )
    else:
        A = np.empty((m, n))
        for i in xrange(m):
            A[i] = struct.unpack("{}{:d}{}".format(endian, n, dtype), f.read(n * tsize))
        return A
