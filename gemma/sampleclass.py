from .readfuncs import *
from .gemmaclass import *
import numpy as np
import pandas
import datetime as dt
import os
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde

dt.datetime.strptime("2012-01-01", "%Y-%m-%d")  # dummy call for weird datetime bug


def percentile(A, pct=0.5, W=1, method="interpolate"):  # [.25,.50,.75]
    """Calculate weighted percentiles of A, given weights in W

    Parameters
    ----------
    A : MxN (masked) array of values to determine percentiles on
    pct : list of P percentiles to be determined
    W : Mx1 array of weigths
    method : what to return if there is no exact percentile?
        !!only interpolate implemented: weighted mean of two surrounding values

    Returns
    -------
    P : PxN array of calculated percentiles
    """
    A = pandas.DataFrame(A)
    W = pandas.Series(W, name="WGHT", dtype=float)
    pct = np.atleast_1d(pct)
    # percentages or fractions?
    if pct.max() > 1.0:
        pct = pct / 100.0
    P = np.empty((pct.shape[0], A.shape[1]))

    # sort A low-high for each column
    ic = 0
    for c in A:
        C = pandas.concat((A[c], W), axis=1, keys=[c, "WGHT"])
        C = C[C[c].notnull()]  # exclude missing data
        # still rows in C?
        if C.shape[0] > 0:
            # C.sort(columns=[c],axis=0,inplace=True)
            C = C.sort_values(by=[c], axis=0)
            CW = C["WGHT"].cumsum(axis=0) / C["WGHT"].sum(axis=0)
            for ip, p in enumerate(pct):
                # determine values at pct items (by iterpolation)
                if np.any(CW == p):
                    pa = C[c][CW == p].mean()
                elif CW.min() >= p:
                    pa = C[c].min()
                else:
                    try:
                        plow = CW[CW <= p].max()
                        phigh = CW[CW >= p].min()
                        clow = C[c][CW == plow].values[0]
                        chigh = C[c][CW == phigh].values[0]
                        pa = clow + (p - plow) / (phigh - plow) * (chigh - clow)
                    except:
                        print(p)
                        print(CW)
                        raise
                P[ip, ic] = pa
        else:
            P[:, ic] = np.nan
        ic += 1
    return pandas.DataFrame(P, index=pct, columns=A.columns)


def get_comb(S):
    emc = []
    for i in range(len(S)):
        if S.ix[i] > 0:
            emc.append(S.index[i])
    return ", ".join(emc)


class sample(object):
    """ Class that contains one g-emma sample run

        Reads in sample header information and further acts as an iterator object along datarows. Data from samples are not read by default.
        
        Data can be accessed as sample[column]

        Parameters
        ----------
        gemma : gemma-object
        read_data : bool. Read data (default False)

        Attributes
        ----------
        gemma : gemma object
            parent gemma object
        endmembers : list of strings
            end-member names
        name : string
            name of sample (sample code)
        date : datetime
            date/time information of sample
        nbehave : int
            number of behavioural samples
        data : pandas.DataFrame
            sample data
        
        """

    def __init__(self, gemma=None, read_data=False):
        self.gemma = gemma
        self.endmembers = self.gemma.endmembers
        try:
            self.name = readstring(self.gemma.f, endian=self.gemma.endian)
            date = readstring(self.gemma.f, endian=self.gemma.endian)
            try:
                self.date = dt.datetime.strptime(date, "%Y-%m-%dT%H:%M:%S")
            except:
                print("Wrong date notation (should be YYYY-MM-DDThh:mm:ss): %s" % date)

            self.nbehave = readlong(self.gemma.f, endian=self.gemma.endian)
            self.nrows = readint(self.gemma.f, endian=self.gemma.endian)
            self.data_pos = self.gemma.f.tell()
            self.cur_pos = self.gemma.f.tell()
            self.cur_row = 0
            self.has_data = False
            self.data = None
            self.rowsize = 4 * (
                4
                + (self.gemma.nendmembers + 1) * self.gemma.nsolutes
                + self.gemma.nendmembers
            )
            if self.gemma.diag:
                self.rowsize += 4 * (self.gemma.nsolutes + 1) + 1
            if self.gemma.version == 52:
                self.rowsize -= 4
            iter(self)
            if read_data:
                self.read_data()
            else:
                self.gemma.f.seek(self.data_pos + self.nrows * self.rowsize)
        except struct.error:
            raise
        self.pdf = None
        self.pdfbins = None
        self.cdf = None
        self.ranges = None

    def read_data(self, nrows=None):
        """ Read data. Not done by default

        Parameters
        ----------
        nrows : int, no of rows to read from sample data. Use None (default) for all
        """
        if nrows is None:
            nrows = self.nrows

        if self.nbehave:
            self.gemma.f.seek(self.data_pos)
            self.data = []
            if self.gemma.diag:
                ncols = (
                    4
                    + (self.gemma.nendmembers + 1) * self.gemma.nsolutes
                    + self.gemma.nendmembers
                    + 1
                    + self.gemma.nsolutes
                    + 1
                )
                if self.gemma.version == 52:
                    ncols -= 1
                fmt = "{}3i{:d}f?" % (ncols - 4)
            else:
                ncols = (
                    3
                    + (self.gemma.nendmembers + 1) * self.gemma.nsolutes
                    + self.gemma.nendmembers
                    + 1
                )
                if self.gemma.version == 52:
                    ncols -= 1
                fmt = "3i%if" % (ncols - 3)

            fmt = "".join([fmt for i in range(nrows)])
            fmt = "{}{}".format(self.gemma.endian, fmt)
            data = struct.unpack(fmt, self.gemma.f.read(nrows * self.rowsize))
            for i in range(self.nrows):
                self.data.append(data[i * ncols : i * ncols + ncols])
            del data
            self.data = pandas.DataFrame(self.data, columns=self.gemma.columns)
            self.data = self.data.replace(-9999.0, np.nan)
            # set NA in fractions to 0
            self.data[self.gemma.endmembers] = self.data[self.gemma.endmembers].fillna(
                0
            )
            if self.gemma.version == 52:
                self.data["LH"] = 1.0
            self.has_data = True

    def percentile(self, pct=[0.25, 0.50, 0.75], params=None, mask=None):
        """Return DataFrame with values of params @ percentiles, weighed by likelihood (LH) column
            
        Parameters
        ----------
        pct : list of percentiles
        params : list of parameters to calculate (default: all)
        mask : boolean Series or ndarray to select data

        Returns
        -------
        p : DataFrame PxN array of calculated percentiles
        """
        if not self.has_data:
            self.read_data()
        if params == None:
            params = filter(lambda item: not (item == "LH"), self.gemma.columns)
        if isinstance(mask, (pandas.Series, np.ndarray)):
            return percentile(self.data[params][mask], pct, self.data["LH"])
        else:
            return percentile(self.data[params], pct, self.data["LH"])

    def em_contrib(self):
        """Function returns Series with frequencies of occurrence in the behavioural runs for each separate endmember
            and all possible end-member combinations"""
        if not self.has_data:
            self.read_data()
        self.data["emcomb"] = self.data[self.endmembers].apply(get_comb, axis=1)
        emc = (self.data[self.endmembers] > 0).sum()
        evc = self.data["emcomb"].value_counts()
        emcontrib = pandas.concat((emc, evc))
        emcontrib.name = self.name
        return emcontrib / float(len(self.data))

    def create_pdf(self, nbins=100, ranges=None):
        """Return pdf for all columns in self.data"""
        if not self.has_data:
            self.read_data()

        if ranges is None:
            ranges = {}

        cols = [
            c for c in self.gemma.columns if c not in ["ITER", "NENDM", "NSOL", "LH"]
        ]

        self.pdf = pandas.DataFrame(np.zeros((nbins, len(cols))), columns=cols)
        self.pdfbins = pandas.DataFrame(np.zeros((nbins + 1, len(cols))), columns=cols)

        # loop through columns
        sumLH = self.data["LH"].sum()
        for col in cols:
            if col not in ranges:
                ranges[col] = (self.data[col].min(), self.data[col].max())

            bins = np.linspace(ranges[col][0], ranges[col][1], nbins + 1)
            bins[0] -= 0.001
            cut = pandas.cut(self.data[col], bins, labels=False)
            grp = self.data["LH"].groupby(cut)
            self.pdf[col] = grp.sum() / sumLH
            self.pdfbins[col] = bins
        self.ranges = ranges

    def create_cdf(self):
        if self.pdf is None:
            print("No pdf, run create_pdf first")
            return None

    def plot_residuals(self):
        """Plot boxplot of residuals"""
        if self.gemma.diag:
            if self.data == None:
                self.read_data()

            fig = plt.figure()
            ax = fig.add_subplot(111)
            cols = ["Res_WB"]
            for s in self.gemma.solutes:
                cols += ["Res_" + s]
            d2 = self.data[cols]
            box = ax.boxplot(d2.values)
            ax.hlines(0, 0, len(cols))
            ax.set_xticklabels(cols)
            plt.title("Residuals for Sample " + self.name, fontsize="large")
            plt.show()
        else:
            print("No residuals to plot, not in diagnostics mode")

    def boxplot(self, params, violin=True):
        """ Create boxplot or violin plot

        Parameters
        ----------
        params : list of parameters from data to draw
        violin : bool, draw violin plot around boxplot if True (default)

        Examples
        --------
        .. plot:: pyplots/boxplot.py

        """
        # code from http://pyinsci.blogspot.nl/2009/09/violin-plot-with-matplotlib.html
        if not self.has_data:
            self.read_data()
        assert self.has_data

        fig = plt.figure()
        ax = fig.add_subplot(111)
        pos = range(len(params))
        if violin:
            dist = len(params)
            w = 0.9 * min(0.15 * max(dist, 1.0), 0.5)
            for prm, p in zip(params, pos):
                k = gaussian_kde(self.data[prm].values)
                m = k.dataset.min()
                M = k.dataset.max()
                x = np.arange(m, M, (M - m) / 100.0)
                v = k.evaluate(x)
                v = v / v.max() * w
                ax.fill_betweenx(x, p, v + p, facecolor="y", alpha=0.3)
                ax.fill_betweenx(x, p, -v + p, facecolor="y", alpha=0.3)
        ax.boxplot(self.data[params].values, notch=1, positions=pos, vert=1)
        ax.set_xticklabels(params)
        plt.show()

    def dottyplot(self, params):
        """Function to draw dotty plot of given parameters

        Parameters
        ----------
        params : list of parameters from data to draw

        Examples
        --------
        .. plot:: pyplots/dottyplot.py

        """
        if not self.has_data:
            self.read_data()
        assert self.has_data

        fig = plt.figure()
        nplots = len(params)
        if nplots > 3:
            nr = 2
            nc = int((nplots + 1) / 2)
        else:
            nr = 1
            nc = nplots
        ax = []
        for i, p in enumerate(params):
            if "_" in p:
                e, s = p.split("_")
            else:
                e = p
                s = "fraction"

            if i:
                ax += [fig.add_subplot(nr, nc, i + 1, sharey=ax[0])]
            else:
                ax += [fig.add_subplot(nr, nc, i + 1)]
            ax[-1].plot(self.data[p], self.data["LH"], ".")
            if p in self.gemma.endmembers:
                ax[-1].set_xlim(0, 1)
            ax[-1].set_title("%s (n=%i)" % (p, self.data[p].count()))
        fig.suptitle("Sample: " + self.name, fontsize="x-large")
        plt.show()

    # overload []: pass to self.data
    def __getitem__(self, key):
        if not self.has_data:
            self.read_data()
        assert self.has_data
        return self.data[key]

    def __setitem__(self, key, value):
        if not self.has_data:
            self.read_data()
        assert self.has_data
        self.data[key] = value
