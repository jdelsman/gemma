# import gemmaclass
from .gemmaclass import gemma

# import sampleclass
from .sampleclass import sample

from gemma import plotting, gemmafiles
