from setuptools import find_packages, setup

with open("README.md") as f:
    long_description = f.read()

setup(
    name="gemma",
    description="G-EMMA: end-member mixing analysis within a GLUE uncertainty assessment framework",
    long_description=long_description,
    url="https://gitlab.com/jdelsman/gemma",
    author="Joost Delsman",
    author_email="joost.delsman@deltares.nl",
    license="MIT",
    packages=find_packages(),
    package_dir={"gemma": "gemma"},
    package_data={},  # "imod": ["templates/*.j2", "templates/mf6/*.j2"]},
    test_suite="tests",
    use_scm_version=True,
    setup_requires=["setuptools_scm"],
    python_requires=">=3.6",
    install_requires=["matplotlib", "numpy", "pandas", "scipy",],
    extras_require={
        "dev": [
            "black",
            "pytest",
            "pytest-cov",
            "pytest-benchmark",
            "sphinx",
            "sphinx_rtd_theme",
        ],
        "optional": [],
    },
    classifiers=[
        # https://pypi.python.org/pypi?%3Aaction=list_classifiers
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering :: Hydrology",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: Implementation :: CPython",
    ],
    keywords="G-EMMA endmember mixing analysis GLUE uncertainty",
)
